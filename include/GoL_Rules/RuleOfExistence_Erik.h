/**
 * @file RuleOfExistence_Erik.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H
#define GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H

#include "RuleOfExistence.h"

/**
 * @class RuleOfExistence_Erik
 * @brief Rule based on Conway's rule but which also differentiates the appearance of cells based on their age.
 * @details Concrete Rule of existence class, implementing Erik's rule.
 *
 * The amount of alive neighbours for the rule are as follows:
 * <table>
 * <tr><th>UNDERPOPULATION</th><th>OVERPOPULATION</th><th>RESURRECTION</th></tr>
 * <tr><td>< 2</td><td>> 3</td><td>= 3</td></tr>
 * </table>
 *
 * Sentient lifeforms is rarely created, but once in a while a cell has lived enough generations to become as wise as Erik.
 * - Once a cell has survived a minimum amount of generations it will recieve a color to distinguish itself from younger ones.
 * - If such a cell would then survive another set amount of generations, it will be marked with a value of **E**.
 * - In the extreme case, where the cell has achieved above requirements and is determined to be the oldest living cell, it will
 * become a **primeElder**, and have its color changed once again. A generation may only have one such elder.
 *
 * @test Does the simulation follow the rules correctly?
 * @todo Does bug exist if (only one elder cell exists) -> (it is appointed the prime elder when no other elder exists)
 * -> (prime elder will die the turn after) -> (ELDER color is not set/reset to OLD)
 */
class RuleOfExistence_Erik : public RuleOfExistence
{
private:
    char usedCellValue;	/**< char value to differentiate very old cells.*/
    Cell* primeElder; /**< Pointer to the cell considered the oldest in the population*/

    /**
     * @brief Applies the specific erikfy-rules on a cell
     * @details If the cell is not due to be killed some additional rules will affect it.
     * - A cell older than 4 will take on the specified (in **STATE_COLORS**-const) color showing its old.
     * - A cell older than 9 will be displayed with the char specified in the data member **usedCellValue**
     * - If the cell is older than the previously appointed **primeElder**, the cell will take on that role.
     *
     * @param cell reference to the cell rule will be applied on.
     * @param action The action expected to be taken on the cell in the next generation.
     * @test Are the rules applied correctly?
    */
    void erikfyCell(Cell& cell, ACTION action);

    /**
     * @brief Appoints the new elder.
     * @details Method changes back the color on previous elder before appointing the new elder and it's new "elder" color.
     * @param newElder A reference to the cell to be considered the elder.
     * @test Is previous elder cell-color reset properly?
     * @test Does the new elder work as expected?
    */
    void setPrimeElder(Cell* newElder);

public:
    /**
     *@brief constructor
     *@details The rule is initiated with the same rule-conditions as in RuleOfExistence_Conway.
     * The special character used for very old cells is set to 'E' and the **primeElder** is set to be no one(nullptr).
     *@param cells Reference to the cell-collection the rule is to be applied on.
     *@test Is object created as expected?
    */
    RuleOfExistence_Erik(map<Point, Cell>& cells)
            : RuleOfExistence({2,3,3}, cells, ALL_DIRECTIONS, "erik"), usedCellValue('E') {
        primeElder = nullptr;
    }

    /**
     * @brief destructor
     */
    ~RuleOfExistence_Erik() {}

    /**
     * @brief Executes rule on the cells
     * @details Method loops through the cell-collection applying the rules specified in the class on all cells that are not rim-cells.
     * @test Have all the rules been applied correctly?
     * @test Have all cells been affected that are expected to?
    */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H
