/**
 * @file RuleFactory.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef RULEFACTORY_H
#define RULEFACTORY_H

#include "GoL_Rules/RuleOfExistence.h"

/**
 * @class RuleFactory
 * @brief Singleton class that handles creation of RulesOfExistence objects.
 * @details Creates and returns the rules that are used in Population.
 * It begins by first instantiating a RuleFactory which after given the map of cells (from Population)
 * and the name of the desired ruleset returns a new instance of a (derived, since the base is abstract)
 * RuleOfExistence-class. Which ruleset is returned is hardcoded by matching the name of the rule in createAndReturnRule.
 *
 * The returned rules are then executed in Population through pointers. Because the rules have access to the map
 * of cells in Population through a reference (see createAndReturnRule()) they can directly affect each Cell in the map.
 *
 * @test Test what the class returns for an invalid ruleName.
 */
class RuleFactory
{
private:
    /**
     * @brief RuleFactory constructor.
     * @details Constructor is defined as private. This to prevent public usage which would compromise the integrity of the singleton.
     */
    RuleFactory() {}

public:
    /**
     * @brief Returns an instance of a RuleFactory.
     * @details Returns a pointer to a static instance of the class.
     * @return Reference to a RuleFactory-instance.
     * @test That only one instance is created.
     */
    static RuleFactory& getInstance();

    /**
     * @brief Creates and returns a specified RuleOfExistence.
     * @details The function constructs the rule that is specified in the **ruleName**. If the rule specified doesn't
     * match any of the strings in the function then it defaults to Conway's rules. The returned pointer makes it
     * possible for Population to execute the rules on the cells by **executeRule()** defined in derived classes of
     * RuleOfExistence.
     * @param cells: A map of cells from Population. The key is a Point in the simulation, which gives access to a Cell.
     * @param ruleName: The rule that should be instantiated.
     * @return A pointer to a derived and instantiated RuleOfExistence-class.
     * @test That the right rule is returned for the correct parameters.
     * @test That it doesn't return a nullptr.
     */
    RuleOfExistence* createAndReturnRule(map<Point, Cell>& cells, string ruleName = "conway");
};

#endif