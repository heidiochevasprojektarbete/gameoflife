/**
 * @file RuleOfExistence_Conway.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
#define GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H

#include "RuleOfExistence.h"

/**
 * @class RuleOfExistence_Conway
 * @brief Conway's Rule of Existence
 * @details A derived class from the abstract base class RuleOfExistence.
 * This class implements Conway's Rule of Existence by setting the PopulationLimits
 * and Directions and defining the executeRule()-function.
 *
 * The amount of alive neighbours for the rule are as follows:
 * <table>
 * <tr><th>UNDERPOPULATION</th><th>OVERPOPULATION</th><th>RESURRECTION</th></tr>
 * <tr><td>< 2</td><td>> 3</td><td>= 3</td></tr>
 * </table>
 *
 * At under- and overpopulation the cell dies if it is alive.
 * It is resurrected if the amount of alive cells around it is 3.
 *
 * All 8 directions are checked for live cells.
 *
 * @test Is the rule applied as expected?
 */
class RuleOfExistence_Conway : public RuleOfExistence
{
private:

public:
    /**
     * @brief Constructor for Conway's Rule of Existence
     * @details Sets the population limits for the RuleOfExistence as < 2 (underpopulation),
     * \> 3 (overpopulation) and resurrection at 3 live neighbours. All neighbours of a cell in
     * all 8 directions are checked with the constant global _ALL_DIRECTIONS_.
     * @param cells: A reference to the cells in Population, mapped in a grid by position.
     * @test Can it be constructed with empty an empty Population of cells?
     * @todo Should it throw if cells is empty?
     */
    RuleOfExistence_Conway(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, ALL_DIRECTIONS, "conway") {}

    /**
     * @brief Destructor for Conway's Rule of Existence.
     */
    ~RuleOfExistence_Conway() {}

    /**
     * @brief Executes Conway's Rule of Existence.
     * @details Iterates through the map of cells from Population and counts the amount of live neighbours
     * for cells that aren't rim cells (on the outer limit of the world in the grid). The action for the cell is
     * then determined by the amount of live neighbours with **getAction()** from the base class.
     *
     * Depending on the action returned, the color state of the cell is set with **setNextColor()** and the action
     * is set to the cell with **setNextGenerationAction()**.
     * @test Is the rule executed as expected? (Chuck different cell patterns at it.)
     */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
