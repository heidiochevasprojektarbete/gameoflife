/**
 * @file RuleOfExistence.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/
#ifndef RULEOFEXISTENCE_H
#define RULEOFEXISTENCE_H

#include<string>
#include<map>
#include<vector>
#include "Cell_Culture/Cell.h"
#include "Support/Globals.h"
using namespace std;

/**
 * @struct PopulationLimits
 * @brief Data structure for storing population limits.
 * @details Used by the derived RuleOfExistence-classes to decide which ACTION to take after counting the
 * amount of neighbours of a Cell in a Population.
 */
struct PopulationLimits {
    int UNDERPOPULATION, /**< cell dies of loneliness */
            OVERPOPULATION, /**< cell dies of overcrowding */
            RESURRECTION; /**< cell lives on/is resurrected */
};

/**
 * @struct Directions
 * @brief Data structure for storing directional values.
 * @details Directions can be presented in the form of {0, -1}, {1, 0} etc. where the first value is horizontal
 * (column) offset and the second value the vertical (row) offset from the cell in the middle.
 * These values could be used to tell us the direction of a neighbouring cell.
 * Example:
 * <table>
 * <tr>
 * <td>-1, -1</td>
 * <td>0, -1</td>
 * <td>1, -1</td>
 * </tr>
 * <tr>
 * <td>0, -1</td>
 * <td><b>0, 0</b></td>
 * <td>1, 0</td>
 * </tr>
 * <tr>
 * <td>-1, 1</td>
 * <td>0, 1</td>
 * <td>1, 1</td>
 * </tr>
 * </table>
 * The Cell itself is in the center, {0, 0}.
 *
 * North {0, -1} is in the same column (0) as the cell, but one row above (-1).
 * East {1, 0} is one column past the cell in the center (1), but on the same row (0).
 *
 * So -1 and 1 could be short for before and past the cell, where the horizontal and vertical
 * are column and row respectively.
 */
struct Directions {
    int HORIZONTAL /**Value can be -1, 0 or 1*/, VERTICAL; /**< Value can be -1, 0 or 1*/
};

/**
 * @brief A vector containing Directions-structs.
 * @details This vector contains all directions that are considered neighbours of a cell in a grid.
 * (N, E, S, W, NE, SE, SW, NW)
 */
const vector<Directions> ALL_DIRECTIONS{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 },{ 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

/**
 * @brief A vector containing cardinal Directions.
 * @details Contains the cardinal Directions-structs (N, E, S, W or up, right, down, left).
 */
const vector<Directions> CARDINAL{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 } };

/**
 * @brief A vector containing diagonal Directions.
 * @details Contains the diagonal Directions-structs (NE, SE, SW, NW).
 */
const vector<Directions> DIAGONAL{ { 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

/**
 * @class RuleOfExistence
 * @brief Abstract base class for the rules of the game.
 * @details An abstract base class that defines the skeleton for a rule of existence in the Game of Life.
 * The rule will have a name and a reference to a map with cells in a gridlike positioning system.
 *
 * It will also have the population limits set for underpopulation, overpopulation and resurrection, which
 * are some of the main rules of the game. The cell who is alive while having
 * an underpopulation or overpopulation of neighbours dies, while a dead cell is resurrected with a resurrection-amount
 * of cells as neighbours.
 *
 * This rule also contains the directions the rule should look at when counting the amount of live cells.
 *
 * The usual rule execution path is executeRule() -> countAliveNeighbours() (inside executeRule()) for a cell ->
 * getAction() (inside executeRule()) for the cell.
 *
 * @test Should not be able to construct or return abstract class.
 */
class RuleOfExistence {
protected:
    string ruleName; /**< The name of the rule as a string */

    map<Point, Cell>& cells; /**< Reference to the population of cells */

    const PopulationLimits POPULATION_LIMITS; /**< The limits for underpopulation, overpopulation and resurrection in the
 * simulation*/

    const vector<Directions>& DIRECTIONS; /**< The directions (by reference), by which neighbouring cells are identified */

    /**
     * @brief Counts the alive neighbours around a point.
     * @details Begins by selecting the x and y coordinates of a point and then iterating through the Directions
     * (specified in the rule) counting the amount of cells that are still alive around the center point.
     * @param currentPoint: The center point for which neighboring living cells are counted.
     * @return The amount of neigbours that are alive.
     * @test Should return 0 if all neighbours checked are dead.
     * @test Should return the amount of directions in the rule if all neighbours are alive.
     * @test Can currentPoint contain negative x- and y-values?
     * @todo Should it throw if negative values are present in the input parameter?
     */
    int countAliveNeighbours(Point currentPoint);

    /**
     * @brief Returns the action (for a cell) as dictated by the rules.
     * @details Follows the rules in the Game of Life when deciding the next action for a cell.
     * If a cell is alive and the neighbours around it lead to over- or underpopulation (as decided in POPULATION_LIMITS),
     * it is killed off with KILL_CELL. Otherwise the live cell is ignored with IGNORE_CELL, incrementing its age.
     *
     * If the cell is dead and the neighbours would lead it to be resurrected (decided by the RESURRECTION
     * limit in POPULATION_LIMITS), it will be given the action GIVE_CELL_LIFE.
     *
     * If neither apply, the action will be DO_NOTHING.
     *
     * @param aliveNeighbours: The amount of live neighbours around the cell.
     * @param isAlive: Whether or not the cell in question is alive.
     * @return The action that the cell should have.
     * @test Positive and negative values for aliveNeighbours.
     * @test TRUE or FALSE values for isAlive.
     * @test Test both positive and negative values and TRUE/FALSE at the same time.
     */
    ACTION getAction(int aliveNeighbours, bool isAlive);

public:
    /**
     * @brief The constructor for RuleOfExistence.
     * @details Takes four different parameters to set the value of it's private data members which in turn help
     * define some of the rule structure.
     * @param limits: Limits that in conjunction with ruling decide the fate of a cell. UNDERPOPULATION,
     * OVERPOPULATION, RESURRECTION
     * @param cells: A reference to the map with cells from Population. Is required to execute rules on the cells.
     * @param DIRECTIONS: A reference to which directions the rule should check for live cells.
     * @param ruleName: The string name of the rule that is being constructed.
     * @test Negative population limits.
     * @test Empty cells-map.
     * @test Directions further away from the center point.
     * @test Random strings for ruleName.
     * @todo Should it throw for negative limits?
     */
    RuleOfExistence(PopulationLimits limits, map<Point, Cell>& cells, const vector<Directions>& DIRECTIONS, string ruleName)
            : POPULATION_LIMITS(limits), cells(cells), DIRECTIONS(DIRECTIONS), ruleName(ruleName) {}

    /**
     * @brief: Virtual destructor for the abstract base class.
     */
    virtual ~RuleOfExistence() {}

    /**
     * @brief Virtual function for executing a rule.
     * @details Uses the template method design pattern.
     */
    virtual void executeRule() = 0;

    /**
     * @brief Returns the name of the rule.
     * @return The name of the rule.
     * @test Is the name returned?
     */
    string getRuleName() { return ruleName; }
};

#endif