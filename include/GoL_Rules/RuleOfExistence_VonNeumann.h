/**
 * @file RuleOfExistence_VonNeumann.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
#define GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H

#include "RuleOfExistence.h"

/**
 * @class RuleOfExistence_VonNeumann
 * @brief Rule where only the four cardinal neighbours are accounted for.
 * @details A rule which only uses the cardinal directions (N, E, S, W) when checking for live neighbours for cells.
 * Otherwise it has the same rules as Conway for underpopulation, overpopulation and resurrection.
 *
 * <table>
 * <tr><th>UNDERPOPULATION</th><th>OVERPOPULATION</th><th>RESURRECTION</th></tr>
 * <tr><td>< 2</td><td>> 3</td><td>= 3</td></tr>
 * </table>
 *
 * Derives from abstract base class **RuleOfExistence**.
 * @test Are the rules followed as expected?
 */
class RuleOfExistence_VonNeumann : public RuleOfExistence
{
private:

public:
    /**
     * @brief Constructor for Von Neumann's rule.
     * @details Initializes the Rule of Existence with underpopulation at < 2, overpopulation at > 3 and resurrection
     * at exactly 3 live neighbours in the cardinal directions, north, east, south, west. This rule can be instanced
     * once through a RuleFactory by using the string von_neumann.
     * @param cells: A reference to a Population of cells mapped to a grid-like system which will be updated in
     * accordance with the Von Neumann-rules.
     * @test Are the rules followed as expected?
     */
    RuleOfExistence_VonNeumann(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, CARDINAL, "von_neumann") {}

    /**
     * @brief destructor
     */
    ~RuleOfExistence_VonNeumann() {}

    /**
     * @brief Executes Von Neumann's rule
     * @details Updates the cells according to Von Neumann's rules. Iterates through each (non rim-)cell in the map
     * checking for the amount of live neighbours in the cardinal directions.
     * @test Are the rules followed as expected?
     * @test Are only the cardinal directions checked?
     */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
