/**
 * @file GameOfLife.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef GameOfLifeH
#define GameOfLifeH

#include "Cell_Culture/Population.h"
#include "ScreenPrinter.h"
#include <string>

/**
 * @class GameOfLife
 * @brief The heart of the simulation, interconnects the main execution with the graphical presentation.
 * @details Creates and manages Population and the ScreenPrinter instances. Will instruct the Population of cells to keep
 * updating as long as the specified number of generations is not reached, and for each iteration instruct
 * ScreenPrinter to write the information on screen.
*/
class GameOfLife {

private:
    Population population; /**< The Population which the simulation is performed on*/
    ScreenPrinter& screenPrinter; /**< Reference to a screenPrinter*/
    int nrOfGenerations; /**< Number of generations to simulate before end*/

public:
    /**
     * @brief constructor
     * @details The constructor uses the passed values to set up the game-options as well as initiate a population.
     * @param nrOfGenerations Integer determining for how long the simulation will run.
     * @param evenRuleName String value of a rule-name to be used on even numbered generations.
     * @param oddRuleName String value of a rule-name to be used on odd numbered generations.
     * @test Is environment set up as intended?
    */
    GameOfLife(int nrOfGenerations, string evenRuleName, string oddRuleName);

    /**
     * @brief Returns the current generation in the simulation.
     * @details This function was made to test the passing of generations of a GameOfLife-instance.
     * GameOfLife asks for the generation number through a custom accessor from Population.
     * @return The current generation.
     * @test Is the generation number returned?
     */
    int getCurrentGeneration() {return population.getGeneration();}

    /**
     * @brief Starts the simulation.
     * @details Sets up the simulation by clearing the terminal and printing generation 0.
     * Then enters a loop which:
     *    calculates a new generation ->
     *    prints it to terminal ->
     *    and sleeps for 100 milliseconds.
     * This is repeated until the Population has lived for the number of generations specified in the constructor.
     * @test Is correct number of generations simulated?
     * @todo Simulation should be aborted if the population has reached an unchangeable(static) state before reaching specified number
     * of generations.
    */
    void runSimulation();

};

#endif
