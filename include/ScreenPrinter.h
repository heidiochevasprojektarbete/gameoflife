/**
 * @file ScreenPrinter.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/



#ifndef screenPrinterH
#define screenPrinterH


#include "../terminal/terminal.h"
#include "Cell_Culture/Population.h"

/**
 * @class ScreenPrinter
 * @brief Responsible for visually representing the simulation world on screen.
 * @details This class is responsible for all output to screen, including the simulation,
 * printed messages and clearing the screen. Class operates as a singleton
 * @test Do all functions work as expected?
 */
class ScreenPrinter {

private:
    Terminal terminal; /**< A Terminal that handles colored text and a placement-cursor in console.*/

    /**
     * @brief constructor.
     * @details Private constructor to protect the class's ability to function as a singleton.
     */
    ScreenPrinter() {}

public:
    /**
     * @brief A ScreenPrinter-factory.
     * @details Constructs only once and after returns an instance of the ScreenPrinter.
     * @return Instance of ScreenPrinter.
     */
    static ScreenPrinter& getInstance() {
        static ScreenPrinter instance;
        return instance;
    }

    /**
     * @brief Prints the board with cells to console.
     * @details This function has the responsibility of printing the simulated data to the screen in GameOfLife.
     *
     * It begins by getting the global variable WORLD_DIMENSIONS to decide the
     * amount of rows and columns that the output should have. After that it begins
     * to iterate through the map of cells from Population.
     *
     * For each cell in the Population (that isn't a rim cell) the Terminal
     * sets the console color to match that of the cell. After that the cell's character
     * (aka value) is printed with cout. Each time a cell has been printed out, the color is reset.
     *
     * The cell is printed on a black background (STATE_COLORS.DEAD);
     * @param population: A reference to a Population which contains a map of cells.
     * @test Test with different populations.
     * @todo Should it throw if the referenced population is empty?
     */
    void printBoard(Population& population);

    /**
     * @brief Prints out a help message.
     * @details Prints out the arguments the user of the application can use to change the simulation.
     * This screen is printed after MainArguments has parsed a "-h" as an argument.
     * @test Is the screen printed?
     */
    void printHelpScreen();

    /**
     * @brief Prints a message to the screen.
     * @details Used by MainArguments to print out error messages if a value isn't found.
     * Takes a string as input and prints it out with cout and endl.
     * @param message: The message that is to be printed to the screen.
     * @test Try with different strings.
     */
    void printMessage(string message);

    /**
     * @brief Clears the screen of all characters.
     * @details Used to clear the screen after each printed simulation in GameOfLife (so that it can be printed to again).
     * The screen clearing is implemented in the class Terminal.
     * @test Is the screen cleared?
     */
    void clearScreen();
};

#endif
