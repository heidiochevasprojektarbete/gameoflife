/**
 * @file Globals.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include "SupportStructures.h"

using namespace std;

/**
 * @brief The world dimensions.
 * @details The size of the world the cells inhabit.
 * Contains values for WIDTH and HEIGHT. The default values are {80, 24}.
 *
 * Used in:
 * <ul>
 * <li>ScreenPrinter: To decide the height and width of the window/output.</li>
 * <li>Population: Used to set the limits for rows and columns for a keymap which contains each individual cell.
 * The key is in the form <int, int> and is used to get a Cell by their position.</li>
 * <li>FileLoader: The values for WORLD_DIMENSIONS are set after reading the values from a file. After that the
 * FileLoader can begin to map the Cells.</li>
 * <li>WorldsizeArgument: The values for WORLD_DIMENSIONS are set by passing the size argument.</li>
 * </ul>
 */
extern Dimensions WORLD_DIMENSIONS;

/**
 * @brief The filename.
 * @details This value is set when an argument contains a file name parameter is handled by MainArguments.
 * FileLoader uses this variable when opening and reading a file. Population checks whether or not this
 * string is empty - if it is, then no filename has been supplied and the cell culture is randomized.
 */
extern string fileName;

#endif