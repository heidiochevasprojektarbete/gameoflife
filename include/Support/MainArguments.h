/**
 * @file MainArguments.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef GAMEOFLIFE_MAINARGUMENTS_H
#define GAMEOFLIFE_MAINARGUMENTS_H

#include "Globals.h"
#include "ScreenPrinter.h"
#include <sstream>
#include <string>

using namespace std;

/**
 * @struct ApplicationValues
 * @brief A container for storing relevant information to the conditions for execution.
 * @details The values of the container are modified by execution of
 * the different BaseArgument children.
 */
struct ApplicationValues {
    bool runSimulation = true;  /**< Should the simulation be run? */
    /** String to specify the name of the even rule */
    string evenRuleName, oddRuleName;   /**< String to specify the name of the odd rule */
    int maxGenerations = 100;   /**< Number of generations to simulate before and of game.*/
};

/**
 * @class BaseArgument
 * @brief Abstract class used to derive an argument-class.
 * @details The children of BaseArgument is used to modify the variables which specify the execution parameters.
 *
 * Some argument-classes are used to change the ApplicationValues. Others modify the
 * globals specifying population dimensions or initial-population file.
 *
 * @test getValue should return the content of argValue
 * @test printNoValue should print a message on the screen containing the value of argValue
 *
 * @bug None of the derived classes handles the case of an invalid value.
 */
class BaseArgument {
protected:
    const string argValue; /**< String value initialized by the constructor. Describes type of argument.*/

    /**
     * @brief Prints a message on screen.
     * @details Informs the user that no value was provided for the argument.
     * @test Message should contain which argument was missing a value.
     */
    void printNoValue();

public:
    /**
     * @brief Constructor
     * @details Initializes the private member *argValue*.
     * @param argValue A string representing the type of argument.
     */
    BaseArgument(string argValue) : argValue(argValue) {}

    /**
     * @brief Destructor
     * @details Virtual destructor containing no additional code.
     */
    virtual ~BaseArgument() {}

    /**
     * @brief Applies an argument to the application parameters.
     * @details Pure virtual method which takes a ApplicationValues struct and uses the instructions
     * specified in the child class to change the structures members.
     * @param appValues Pointer to a ApplicationValues struct.
     * @param value String containing the value passed with the argument.
     */
    virtual void execute(ApplicationValues& appValues, char* value = nullptr) = 0;

    /**
     * @brief Returns the argument value
     * @details Returns the argument value which has been set by the child's constructor.
     * @return A pointer to the protected data member.
     * @test A string value bigger than zero-length is returned.
     */
    const string& getValue() { return argValue; }
};


/**
 * Help screen
 * @class HelpArgument
 * @brief Class managing the passing of the help argument
 * @details Class doesn't handle an actual value passed from the user. It only specifies that an instruction
 * to show the help screen has been passed.
 * If this argument is used(executed) there will be no simulation
 *
 * @test If executed a help screen will be printed.
 * @test If executed the application will not run the simulation.
 */
class HelpArgument : public BaseArgument {
public:
    /**
     * @brief Constructor
     * @details Initializes the base class with the value "-h"
     */
    HelpArgument() : BaseArgument("-h") {}

    /**
     * @brief Destructor, no additional commands.
     */
    ~HelpArgument() {}

    /**
     * @brief Sets runSimulation-member in a ApplicationValues struct to false.
     * @details Modifies the passed struct with instructions not to run the simulation.
     * The char* value passed as a parameter is not needed and therefor never used.
     * @param appValues Pointer to a struct which will be modified.
     * @param value Is not used.
     */
    void execute(ApplicationValues& appValues, char* value);
};

/**
 * Amount of generations to simulate
 * @class GenerationsArgument
 * @brief Class managing the argument to specify number of generations
 * @details Class takes an argument which can change the default value of how many generations
 * will be simulated in the game.
 *
 * @test If executed with a value, the maxGenerations-member in a passed struct might have changed.
 * @test No other data member than generation has been modified.
 * @test If executed without value the simulation should not be run.
 * @test If no data, default value for maxGenerations should not have changed.
 *
 * @bug If the value for an argument found isn't an integer, attempts to return string as int and throws with the message stoi (string to int).
 */
class GenerationsArgument : public BaseArgument {
public:
    /**
     * @brief Constructor
     * @details Initializes the base class with the value "-g"
     */
    GenerationsArgument() : BaseArgument("-g") {}

    /**
     * @brief Destructor, no additional commands.
     */
    ~GenerationsArgument() {}

    /**
     * @brief Modifies the maxGenerations-member of the passed struct.
     * @details If passed a value the method changes how many generations is specified in the passed struct.
     * If no value is passed it changes runSimulation to false and prints a message.
     * @param appValues Pointer to a struct which will be modified.
     * @param generations should be an int presented as a char pointer.
     */
    void execute(ApplicationValues& appValues, char* generations);
};


/**
 * Custom population size
 * @class WorldsizeArgument
 * @brief Class managing the argument to specify the size of the simulated world
 * @details A value can be passed to the class which changes the global specifier for how big the simulated
 * and printed grid should be.
 *
 * @test If executed with a value the global should have changed.
 * @test If executed without value the simulation should not be run.
 * @test If no value is given, default value for WORLD_DIMENSIONS should not have changed.
 *
 * @bug 0x0 is a valid world size
 * @bug If argument is incomplete but begins with an int (ex. 100xyz), it sets the number as its WIDTH.
 * @bug Even if input is badly formed runSimulation is not set to false.
 * @bug Invalid characters are read as zeroes, so the string 'potato' gives the dimensions 0x0
 *
 * @todo Throw if argument is badly formed.
 */
class WorldsizeArgument : public BaseArgument {
public:
    /**
    * @brief Constructor
    * @details Initializes the base class with the value "-s"
    */
    WorldsizeArgument() : BaseArgument("-s") {}

    /**
     * @brief Destructor, no additional commands.
     */
    ~WorldsizeArgument() {}

    /**
     * @brief Changes the value of the dimensions global.
     * @details If passed a value, it will be deconstructed into width and height and passed to the global variable.
     * If no value is passed it changes runSimulation to false and prints a message;
     * @param appValues Pointer to a struct which could be modified.
     * @param dimensions Should be a string in the format of 'XxY'
     *
     * @test Parsing of the string should work properly and if not, throw an exception.
     */
    void execute(ApplicationValues& appValues, char* dimensions);
};


/** Initiate population from file
 * @class FileArgument
 * @brief Class managing the passing of a filename as an argument
 * @details A class that is used to take a string filename and change a global variable which
 * will later be used to initiate the population.
 * @test If given a value, the global variable fileName should change.
 * @test If no value is given, simulation should not run.
 * @test If no value is given, fileName (global variable) should be empty.
 */
class FileArgument : public BaseArgument {
public:
    /**
    * @brief Constructor
    * @details Initializes the base class with the value "-f"
    */
    FileArgument() : BaseArgument("-f") {}

    /**
     * @brief Destructor, no additional commands.
     */
    ~FileArgument() {}

    /**
     * @brief Changes the value of the fileName global
     * @details Takes a string value and assigns it to the global.
     * If no value is passed it changes runSimulation to false and prints a message;
     * @param appValues Pointer to a struct which could be modified.
     * @param fileNameArg Search path of file.
     */
    void execute(ApplicationValues& appValues, char* fileNameArg);
};

/** Rule used for even generations
* @class EvenRuleArgument
* @brief Class managing the passing of a even rule argument as a string.
* @details A class that is used to take a string argument and change evenRuleName in the specified struct.
* @test Has the value been changed?
* @test If no value is the simulation stopped from running?
*/
class EvenRuleArgument : public BaseArgument {
public:
    /**
    * @brief Constructor
    * @details Initializes the base class with the value "-er"
    */
    EvenRuleArgument() : BaseArgument("-er") {}

    /**
     * @brief Destructor, no additional commands.
     */
    ~EvenRuleArgument() {}

    /**
     * @brief Changes the value of evenRuleName in specified struct.
     * @details If given a value the method changes the corresponding value in the passed struct.
     * @param appValues Pointer to a struct which will be modified.
     * @param evenRule A string name specifying chosen rule.
     */
    void execute(ApplicationValues& appValues, char* evenRule);
};

/** Rule used for odd generations
* @class OddRuleArgument
* @brief Class managing the passing of a odd rule argument as a string.
* @details A class that is used to take a string argument and change oddRuleName in the specified struct.
* @test Has the value been changed?
* @test If no value is the simulation stopped from running?
*/
class OddRuleArgument : public BaseArgument {
public:
    /**
    * @brief Constructor
    * @details Initializes the base class with the value "-or"
    */
    OddRuleArgument() : BaseArgument("-or") {}

    /**
     * @brief Destructor, no additional commands.
     */
    ~OddRuleArgument() {}

    /**
     * @brief Changes the value of oddRuleName in specified struct.
     * @details If given a value the method changes the corresponding value in the passed struct.
     * @param appValues Pointer to a struct which will be modified.
     * @param oddRule A string name specifying chosen rule.
     */
    void execute(ApplicationValues& appValues, char* oddRule);
};

#endif //GAMEOFLIFE_MAINARGUMENTS_H
