/**
 * @file SupportStructures.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef GAMEOFLIFE_SUPPORTSTRUCTURES_H
#define GAMEOFLIFE_SUPPORTSTRUCTURES_H

/**
 * @struct Point
 * @brief Constitutes a single Point in the simulated world.
 * @details Point:s are used as keys for a map of cells in Population.
 * Using the Point-coordinates as a key you can get the corresponding Cell in
 * its position in the world.
 */
struct Point {
    /** The x-coordinate of a Point. */
    int x, y; /**< The y-coordinate of a Point. */

    /**
     * @brief Overloaded operator for comparing the placement of two Points.
     * @details Compares the x and y-coordinates of two points to decide which Point is smaller.
     * A smaller Point has the smallest x-coordinate or the smallest y-coordinate if the two
     * points are on the same row. So {2,1}<{3,1} and {2,1}<{2,2} are true, while {2,1}<{1,2} is false.
     * @param other: The point to compare against.
     * @return
     * TRUE: The first Point is smaller than the second one.
     * FALSE: The first Point is greater than the second one.
     * @test Does it compare negative values correctly?
     */
    bool operator < (const Point& other) const {
        if (x == other.x)
            return y < other.y;
        return x < other.x;
    }

};

/**
 * @struct Dimensions
 * @brief Data structure for the world size.
 * @details Data structure that holds the width and height of the world for the simulation.
 * Used as the data type for the global variable WORLD_DIMENSIONS in Globals.h.
 * The size for WORLD_DIMENSIONS is set when reading from file or if it's passed as an argument when
 * running the application.
 */
struct Dimensions {
    /** The width of the dimension. */
    int WIDTH, HEIGHT; /**< The height of the dimension. */
};


#endif //GAMEOFLIFE_SUPPORTSTRUCTURES_H
