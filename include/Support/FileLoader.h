/**
 * @file FileLoader.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef FileLoaderH
#define FileLoaderH

#include <map>
#include "Cell_Culture/Cell.h"
#include "Globals.h"

using namespace std;

/**
 * @class FileLoader
 * @brief Determines start-up cells for simulation, based on contents of specified file.
 * @details Reads startup values from a specified file, containing values for **WORLD_DIMENSIONS** and state of Cells in Population.
 * Creates the corresponding cells by reading expected 0 or 1 from file.
 *
 * @test Is the cell population loaded correctly?
*/
class FileLoader {

public:
    /**
     *@brief constructor
    */
    FileLoader() {}

    /**
     * @brief Reads the content of found file and generates Cells.
     * @details Has the responsibility of populating a **Population** of cells from a file, if a file is found.
     * Starts off by reading the first line representing a set of dimensions. A grid of 0's and 1's are read
     * and generates cells accordingly dead(0), living(1).
     * @param cells Pointer to a collection of cells used by Population
     * @test Is the population generated as expected.
     * @test What happens if an unexpected char is encountered.
     * @throw Throws if a file specified in the **fileName** global variable can not be found.
     * @todo Should throw if an unexpected char is encountered - right now it just skips the invalid characters,
     * leading to less cells in the map. An idea would be to avoid editing by reference so that if you come across
     * erroneous content, it doesn't affect the Population in the end, but instead discards the changes?
     * @bug If an invalid character (any other than 0 or 1) is encountered in a file while reading it,
     * the character is skipped/ignored, leading to less cells in the target Population.
    */
    void loadPopulationFromFile(map<Point, Cell>& cells);

};

#endif
