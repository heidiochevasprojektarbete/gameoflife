/**
 * @file MainArgumentsParser.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef mainArgumentsParserH
#define mainArgumentsParserH

#include "MainArguments.h"
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

/**
 * Static functions that parses the starting arguments for the application.
 * @class MainArgumentsParser
 * @brief Handles the parsing of the execution-arguments sent to the application.
 * @details A class with only one public method designed to automate the reading of the start arguments.
 *
 * @test Does the parser correctly identify the arguments passed to it
*/
class MainArgumentsParser {
public:
    /**
     * @brief Performs a parse.
     * @details Method contains internally a set(vector) of allowed argument objects. The method iterates through these and then
     * look within the passed argument-array if it can find corresponding argument. If the argument is found it executes it's
     * argument-objects __*execute*__ method. Some arguments requires an additional value passed from the user. If this is the case the parser
     * tries to extract this value before running the __*execute*__ method.
     *
     * @param argv string array of arguments passed on from the main function.
     * @param length Length of the array sent as first parameter.
     * @return Pointer to a ApplicationValues struct affected by the passed arguments.
     * @test Is every valid argument and value parsed and handled properly?
     *
     * @bug Memory leak occurs if one of the arguments allocated with new throws in the for-loop.
     * @bug Parser sets even- and odd rules even when no simulation is to take place with '-h'.
     * @bug Parser allows passing the same arguments several times, but they are not read since only the first
     * argument is executed.
     *
     * @todo If runSimulation is false, even and odd rules should not be set.
     * @todo If the user inputs arguments of the same kind, they should be notified about it.
    */
    ApplicationValues& runParser(char* argv[], int length);

private:
    ApplicationValues appValues; /**< ApplicationValues struct modified by the parsed arguments*/

    /**
     * @brief Checks if a given option exists
     * @details Given pointers to the borders of the passed arguments-array. The method tries to find an option
     * matching a given valid argument signature.
     * @param begin Pointer to the position in the character array where to begin searching.
     * @param end Pointer to the position in the character array where to stop searching.
     * @param option string to look for representing an argument option.
     * @return a boolean signaling if the given argument option was found.
     * @test Are all valid argument options detected?
    */
    bool optionExists(char** begin, char** end, const std::string& option);

    /**
     * @brief Retrieves the given option value.
     * @details Used similar to __*optionExists*__ but takes the pointer to possible found argument option and retrieves the
     * value following closest after. If the argument option is not found the function returns 0.
     * @param begin Pointer to the position in the character array where to begin searching.
     * @param end Pointer to the position in the character array where to stop searching.
     * @param option string to look for representing an argument option.
     * @return character array of found value or 0 (null) if no value was found.
     * @test If value present is it found and returned.
    */
    char* getOption(char** begin, char** end, const std::string& option);
};

#endif
