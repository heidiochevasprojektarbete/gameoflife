/**
 * @file Cell.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef cellH
#define cellH

#include "../../terminal/terminal.h"

/**
 * @struct StateColors
 * @brief The color states that a cell can have.
 * @details A data structure that holds colors to visualize the state of cells.
 * A cell can be considered **LIVING**, **DEAD**, **OLD** or an **ELDER** cell. A living cell shows up with the color white
 * in the simulation, while a dead cell is colored black. Old and elder cells (which are considered having survived
 * more generations than a merely living cell) can have the colors cyan and magenta.
 */
struct StateColors {
    COLOR LIVING, /**< Representing living cell*/
            DEAD, /**< Representing dead cell*/
            OLD,  /**< Representing old cell*/
            ELDER;/**< Representing very old cell*/
}

/**
 *  @brief The colors for the cells in the simulation.
 *  @details A constant which contains the colors that belong to each state a cell can have in StateColors.
 *  The colors displayed are handled by the Terminal.
 *  Initiates default values.
 */
const STATE_COLORS = { COLOR::WHITE, COLOR::BLACK, COLOR::CYAN, COLOR::MAGENTA };

/**
 * @enum ACTION
 * @brief Enumerator for cell actions.
 * @details Contains the actions that a cell can be subjected to in updateState().
 * **KILL_CELL** kills the (non-rim) cell by setting the age to 0, **IGNORE_CELL** increments the age of the cell by ignoring it and
 * **GIVE_CELL_LIFE** increments the age of the (non-rim) cell by one. A cell is considered to be alive if its age is more than 0.
 * **DO_NOTHING** (which does nothing) is the default next action set after updateState().
 */
// Cell action. Determined by rule, and sent to cell for future change.
enum ACTION { KILL_CELL, IGNORE_CELL, GIVE_CELL_LIFE, DO_NOTHING };

/**
 * @class Cell
 * @brief A single cell in the simulation.
 * @details The cells are entities that contain information and methods concerning their age, whether or not they
 * are a rim cell (= on the outer limit of the world in the simulation) and their character value + color that should be
 * printed to screen.
 *
 * Each cell also contains information about what will occur with them in next generation in the simulation.
 *
 * @test A dead/unborn cell should have an age of 0.
 * @test A live newly born cell should have an age of 1.
 */
class Cell {

private:
    /**
     * @struct CellDetails
     * @brief Details about the cell.
     * @details Contains information pertaining to the cell, such as its age, the color and character it should
     * be printed out with and whether or not it is a rim cell.
     */
    struct CellDetails {	// encapsulate cell details
        int age; /**< The age of the cell. */
        COLOR color; /**< The color the cell should have when written out with Terminal. */
        bool rimCell; /**< Is it a rim cell? */
        char value; /**< The character the cell should be presented with on the screen. */
    } details; /**< Contains the details of a cell (age, color, if it's a rim cell, and character value) */

    /**
     * @struct NextUpdate
     * @brief Information about the next update to the cell.
     * @details Contains information about what will happen to the cell in the next update, such as action,
     * the next color, value and whether or not the cell will still be alive.
     */
    struct NextUpdate {		// encapsulate changes to next state
        ACTION nextGenerationAction; /**< The next action that is taken on the cell. */
        COLOR nextColor; /**< The next color the cell should have. */
        char nextValue; /**< The next value the cell should have. */
        bool willBeAlive; /**< Will the cell be alive in the next update? */
    } nextUpdate; /**< Contains the values that the cell will receive in the next update (color, value, action) */

    /**
     * @brief Increments the age of the cell.
     * @details Increases the age of the cell after each generation in the simulation.
     * @test getAge() returns a value +1 greater after using incrementAge()
     * @todo Should this throw if the cell is a rim cell (immutable)?
     */
    void incrementAge() { details.age++; }

    /**
     * @brief Kills the cell by setting its age to 0.
     * @details Sets the cell's age to 0.
     * @test Cell's age should returned as 0 with getAge() after using killCell().
     * @todo Should this throw if the cell is a rim cell (immutable)? Rim cells aren't alive to begin with.
     * @todo Should this change the color of the cell as well?
     */
    void killCell() { details.age = 0; }

    /**
     * @brief Sets the character for the cell.
     * @details Sets the character which should be printed by ScreenPrinter for the cell in question.
     * @test Should return the set value with a getter.
     * @todo Should this throw if the cell is a rim cell (immutable)?
     * @param value: The character the cell's character should be.
     */
    void setCellValue(char value) { details.value = value; }

    /**
     * @brief Sets the current color of the cell.
     * @details Sets the color that the cell should have in the output of ScreenPrinter.
     * @test Should return the same value if set (for the current update).
     * @todo Should this throw if the cell is a rim cell (immutable)?
     * @param color: The color the cell should currently have.
     */
    void setColor(COLOR color) { this->details.color = color; }

public:
    /**
     * @brief Constructor for a cell that determines its starting values.
     * @details Constructs a cell based on parameters and default values.
     * A cell will always initialize with an age of 0, but if it is alive it will be incremented to 1 after updateState().
     * If the next action is to give a cell life, the cell color state becomes LIVING, otherwise it will be set as DEAD.
     * @test Can a rim cell be given an action?
     * @param isRimCell: If the cell is a rim cell.
     * @param action: What is the initial action for the cell?
     */
    Cell(bool isRimCell = false, ACTION action = DO_NOTHING);

    /**
     * @brief Is the cell alive?
     * @details Returns whether or not the cell is alive. If it is a rim cell it is assumed that it is not alive.
     * If the cell isn't a rim cell it is considered alive if it is older than 0.
     * @return Boolean, indicating yes or no.
     * @test If the cell is a rim cell it should return false.
     * @test If the cell is older than 0 generations, it should return true.
     * @test If the cell is younger than 0 generations (including negative values), it should return false.
     */
    bool isAlive();

    /**
     * @brief Sets the next action for the cell.
     * @details Sets the next action for the cell in the next generation of the simulation.
     * If it is a rim cell or a cell that isAlive() that is given the next action GIVE_CELL_LIFE, the action is not set.
     * Otherwise the new action is set for the cell.
     * @test A rim cell should never have the next action GIVE_CELL_LIFE.
     * @test A live cell should never have the next action GIVE_CELL_LIFE.
     * @param action: The next action the cell should be given.
     */
    void setNextGenerationAction(ACTION action);

    /**
    * @brief Updates cell to new state.
    * @details Changes the state of the cell in the simulation. If the action is KILL_CELL, the cell is killed with killCell().
    * If the action is IGNORE_CELL and the cell isAlive() then its age is incremented. If the action is GIVE_CELL_LIFE and
    * the cell isn't a rim cell, it's age is incremented. If the next value and/or color differ to what the cell has currently
    * then the new value/color is set to the cell.
    * @test nextGenerationAction should always be DO_NOTHING after running this function.
    * @test For each of the four actions test that the result is as expected. (ie. age is incremented, cell is killed, etc.)
    * @test Is the color/value updated? getColor(), getValue()
    */
    void updateState();

    /**
     * @brief Gets the age of the cell.
     * @details Returns the age of the cell, ie. how many generations the cell has survived in the simulation.
     * @test Should be 0 after killCell().
     * @test Should be +1 after incrementAge().
     * @test Should be more than 0 if isAlive() is true. (Cells start with age of 0,
     * but it is incremented in updateState() when given life.)
     * @bug rim-cell can be updated. A rim-cell should always have a static state.
     * @return The age as an integer.
     */
    int getAge() { return details.age; }

    /**
     * @brief Gets the cell's color.
     * @details Returns the cells current color to decide which color to print to the output with ScreenPrinter.
     * @test If color is set it should return the same color with getColor().
     * @return The colour the cell currently has.
     */
    COLOR getColor() { return details.color; }

    /**
     * @brief Is the cell a rim cell?
     * @details Returns whether or not the cell should be considered a rim cell (in the outer limit) in the simulation.
     * @test If a cell is constructed with isRimCell as false, should return false.
     * @test If a cell is constructed with isRimCell as true, should return true.
     * @return
     * TRUE: The cell is a rim cell.
     * FALSE: The cell is not a rim cell.
     */
    bool isRimCell() { return details.rimCell; }

    /**
     * @brief Sets the next color for the cell.
     * @details Sets the colour the cell should have in the next update.
     * @test A set nextColor should be the color set to the cell after updateState().
     * @todo Should the Cell-class have a getNextColor()-getter?
     * @todo Should this throw if the cell is a rim cell (immutable)?
     * @bug Colour can be set on a rimcell.
     * @param nextColor: The next color the cell should have.
     */
    void setNextColor(COLOR nextColor) { this->nextUpdate.nextColor = nextColor; }

    /**
     * @brief Gets the character value of the cell.
     * @details Gets the current character value of the cell.
     * @test The returned value should not be empty after the cell is constructed.
     * @return Returns the character that the cell currently has.
     */
    char getCellValue() { return details.value; }

    /**
     * @brief Sets the next character value for the cell.
     * @details Sets the character that will be printed for the cell with ScreenPrinter in the next update.
     * @test The value that is set with setNextValue() should be the cell's current value after being updated with updateState().
     * @todo Should this throw if the cell is a rim cell (immutable)?
     * @bug Value can be changes on rim-cell. This would compromise the simulation visualisation.
     * @param value: The character that the cell should look like in the next update.
     */
    void setNextCellValue(char value) { nextUpdate.nextValue = value; }

    /**
     * @brief Sets whether or not the cell is alive in the next generation.
     * @details Changes the dead/alive state of the cell depending on the input parameter.
     * @test Set value should be returned correctly with isAliveNext().
     * @todo Should this throw if the cell is a rim cell (immutable)? A rim cell can not be alive.
     * @param isAliveNext: The value to set for the cells living state.
     * TRUE: The cell will be alive in the next update.
     * FALSE: The cell will be dead in the next update.
     */
    void setIsAliveNext(bool isAliveNext) { nextUpdate.willBeAlive = isAliveNext; }

    /**
     * @brief Will the cell be alive in the next generation?
     * @details Returns whether or not the cell will be alive in the next generation of the simulation.
     * The information is retrieved from the nextUpdate-struct in the cell.
     * @test Should return false if the nextGenerationAction is KILL_CELL.
     * @test Should return true if the nextGenerationAction is GIVE_CELL_LIFE.
     * @test Should return true if next color is other than DEAD.
     * @todo Should it throw if a value is not set to nextUpdate.willBeAlive?
     * @return
     * TRUE: The cell will be alive in the next update.
     * FALSE: The cell will be dead in the next update.
     */
    bool isAliveNext() { return nextUpdate.willBeAlive; }

    /**
     * @brief Gets the cells next action.
     * @details Returns a reference to the action that is set to be taken by the cell in the next generation.
     * The information is retrieved from the nextUpdate-struct in the cell.
     * @test If set with setNextGenerationAction then this should return an ACTION.
     * @test The nextGenerationAction in a cell should always be DO_NOTHING after updateState().
     * @return Reference to the next action.
     */
    ACTION& getNextGenerationAction() { return nextUpdate.nextGenerationAction; }

};

#endif
