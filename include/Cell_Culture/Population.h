/**
 * @file Population.h
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#ifndef POPULATION_H
#define POPULATION_H

#include<map>
#include<string>
#include "Cell.h"
#include "Support/Globals.h"
#include "GoL_Rules/RuleOfExistence.h"
#include "GoL_Rules/RuleFactory.h"

using namespace std;

/**
 * @class Population
 * @brief Keeps track of the cells in the simulation.
 * @details Population has complete knowledge of each cells whereabouts in the simulation grid. It keeps track
 * of the current generation and which rules are to be used for the simulation, which it received from RuleFactory
 * in the form of pointers to the rules.
 *
 * Population has the responsibility of updating the cells to their new states according to the even/odd-generation rules.
 * @test Does a population with a size of 1 function as expected?
 * @todo Do memory leaks occur if the application is ended abruptly (rules are deleted in population desctructor)?
 */
class Population
{
private:
    int generation; /**< The current generation */
    map<Point, Cell> cells; /**< All the cells in the simulation, mapped by their position */
    RuleOfExistence* evenRuleOfExistence; /**< Pointer to the even-generation rule */
    RuleOfExistence* oddRuleOfExistence; /**< Pointer to the odd-generation rule */

    /**
     * @brief Randomizes the cell culture
     * @details Creates randomized cells for the simulation. The cells are mapped to the Population with
     * a Point that consists of a column and row. The amount of cells and their positions are decided
     * by the set WORLD_DIMENSIONS.
     *
     * This function has the responsibility of deciding the which cells are rim cells. This is done by checking the position
     * of the cell which is to be initialized if it is on any border/rim point.
     *
     * If the cell isn't a rim cell, then the random generator decides with a result of 0 or 1 if the resulting cell
     * will be dead or alive (by deciding their first action).
     *
     * This function is called if no filename is supplied as a parameter to GameOfLife.
     * @test Does the seed time(0) give different cell cultures each time for the randomization?
     * @test Is the correct amount of cells generated?
     * @test Are the rim cells generated correctly?
     */
    void randomizeCellCulture();

    /**
     * @brief Builds a cell culture from a file.
     * @details Sends the cells-map from Population to a newly instantiated FileLoader that populates it with cells based on
     * the contents in the file. This function is called if a filename is supplied as a parameter to GameOfLife.
     * @test What happens if a fileName has been supplied but the file doesn't exist?
     */
    void buildCellCultureFromFile();

public:
    /**
     * @brief Population constructor.
     * @details Constructs a population for cells starting on generation 0 and with null-pointers to the even/odd-generation rules.
     * @test Does Population point to nullptr after being initialized?
     */
    Population() : generation(0), evenRuleOfExistence(nullptr), oddRuleOfExistence(nullptr) {}

    /**
     * @brief Destructor for Population.
     * @details Frees the allocated memory by deleting the rules.
     */
    ~Population();

    /**
     * @brief Initiates a population
     * @details Initializes the cell culture and fetches the rules (based on the parameter rule names) from the RuleFactory.
     * If a file exists then the cell culture is parsed from the file with a FileLoader, otherwise all cells are randomized
     * before the start of the simulation.
     *
     * If no named odd-generation rule is given, then the even-generation rule is also used for the odd generations.
     * @param evenRuleName: Name for the even-generation rule.
     * @param oddRuleName: Name for the odd-generation rule. Default is "", empty. If not given, then even-rule will also
     * be used for odd-generations.
     * @test If evenRuleName and oddRuleName are the same, is the same rule executed in both cases?
     * @test If the rule name is incorrect, is it handled?
     * @todo Should it throw if rule in string doesn't exist?
     */
    void initiatePopulation(string evenRuleName, string oddRuleName = "");

    /**
     * @brief Calculates the new generation.
     * @details Updates the cell population and decides the generational changes based on the rules.
     * It iterates through the map of cells and calls updateState() for each of them.
     * After updating the cells if executes one of the even/odd rules based on the generation number.
     * @return The new, incremented generation number.
     * @test Generation should be incremented after each run.
     * @test A rule should be executed with each run.
     * @todo Should it throw if the cells-map is empty? (Or is it a pre-condition?)
     */
    int calculateNewGeneration();

    /**
     * @brief Returns a cell based by a specified key value.
     * @details The keys are in the form of Point which contains two ints for a row and column in the cell population.
     * Using this function with a Point-key returns a reference to a Cell with that key.
     * @test Negative values for position? (Is Point for ints or unsigned ints?)
     * @test Values that are outside the height and width for the simulation.
     * @param position: The position of the cell in the map.
     * @return A reference to a cell in the supplied position.
     */
    Cell& getCellAtPosition(Point position) { return cells.at(position); }

    /**
     * @brief Total amount of cells in the population.
     * @details Returns the total amount of cells in the population regardless of their state by counting the size of
     * the cells-map.
     * @test Does it return the correct amount for several different sizes of the world?
     * @test Does it return 1 if the world is 1x1?
     * @return Amount of cells in the population, regardless of state.
     * @todo Function could be const?
     */
    int getTotalCellPopulation() { return cells.size(); }

    /**
     * @brief Returns the current generation.
     * @details Used in generations-tests to check that the generation is updated properly in a GameOfLife-simulation.
     * @return The current generation.
     * @test Is generation returned?
     */
    int getGeneration() {return generation;}

    /**
     * @brief Retrieves the name of applied rule
     * @details If the rule has been crated the function will return it's name. If no rule has been created yet
     * a zero length string is returned. This method has no use in application but is used to perform tests.
     * @return A string value of the rule name.
    */
    string getEvenRuleName(){return (evenRuleOfExistence != nullptr) ?  evenRuleOfExistence->getRuleName() : "";}

    /**
     * @brief Retrieves the name of applied rule
     * @details If the rule has been crated the function will return it's name. If no rule has been created yet
     * a zero length string is returned. This method has no use in application but is used to perform tests.
     * @return A string value of the rule name.
    */
    string getOddRuleName(){return (oddRuleOfExistence != nullptr) ?  oddRuleOfExistence->getRuleName() : "";}
};

#endif