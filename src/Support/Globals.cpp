/**
 * @file Globals.cpp
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#include "Support/Globals.h"

string fileName;
Dimensions WORLD_DIMENSIONS = { 80, 24 };