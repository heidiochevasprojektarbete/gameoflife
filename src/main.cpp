/**
 * @file main.cpp
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/

#include <iostream>
#include "GameOfLife.h"
#include "Support/MainArgumentsParser.h"

#ifdef DEBUG
#include <memstat.hpp>
#endif

using namespace std;

/**
 * @brief The main() of the application.
 * @details The main place where the program begins executing. The program can receive arguments from the terminal/console
 * to decide how the program should behave. The entered arguments are automatically split and counted to the argv and
 * argc parameters. The available commands can be read by writing the -h argument before running the program.
 * Ex. GameOfLife.exe -h
 * @param argc the amount of arguments that are input.
 * @param argv each argument put in a char* array.
 * @return 0, if the application ended successfully.
 */
int main(int argc, char* argv[]) {

    MainArgumentsParser parser;
    ApplicationValues appValues = parser.runParser(argv, argc);

    if (appValues.runSimulation) {
        // Start simulation
        try {
            GameOfLife gameOfLife = GameOfLife(appValues.maxGenerations, appValues.evenRuleName, appValues.oddRuleName);
            gameOfLife.runSimulation();
        }
        catch(ios_base::failure &e){}

    }

    cout << endl;
    return 0;
}
