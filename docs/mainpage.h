// This contains the main page for Doxygen-documentation
/**
 * @mainpage Game of Life
 * This project is about an application simulating Conway's Game of Life, implemented by Erik Ström.
 *
 * @ref about_the_authors <br>
 * @ref general_information
 *
 * @section gameoflife The Game of Life
 * The Game of Life was invented by the mathematician John Conway.
 * It is usually implemented in the form of a grid with cells where a cell may either be dead or alive.
 * Every cell's survival or death is dependent on their neighbours, which are the 8 cells adjacent to the cell.
 * The "game" continues in the form of generations where the state of the cells are decided according to four rules.
 *
 * There are four rules:
 * <i>
 * <ol>
 * <li>A live cell with fewer than two live neighbours dies. (Underpopulation)</li>
 * <li>A live cell with two or more live neighbours lives on to the next generation.</li>
 * <li>A live cell with more than three neighbours dies. (Overpopulation)</li>
 * <li>A dead cell with exactly three live neighbours becomes alive in the next generation. (Reproduction)</li>
 * </ol>
 * </i>
 *
 * <b>Source:</b> <a href="https://en.wikipedia.org/wiki/Conway's_Game_of_Life">Game of Life</a>
 *
 * @section implementation The Implementation
 * This implementation contains (besides Conway's rules) several sets of rules for the Game of Life.
 * The application follows this execution order (image from the assignment):
 * @image html projectimage.png
 *
 * The application starts by parsing input parameters (if these exist) then creates an instance of GameOfLife that acts
 * as an intermediary between the simulation and graphical representation.
 *
 * <b>Source:</b> <i>Projekt - Game of Life</i> by Erik Ström (assignment)
 *
 */

/**
* @page about_the_authors About the Authors
*
* @section author Author
* @subsection Author1 Erik Ström
* Erik (who is currently one of our teachers in the course "Metoder och verktyg i mjukvaruprojekt") has created the implementation of Conway's Game of Life that we're documenting.
*
* @section testers Testers and Documenters
* @subsection Author2 Eva Thilderkvist
* I'm a 28 year old software engineering student at the Mid Sweden University and I am doing my studies through the off-campus program.
* Parallel with my studies I have been travelling and working in the horse racing industry all over the world for the last ten years.
* The interest for programming was initially sparked through mathematics, analysis and problem solving relating to sports gambling.
*
* @subsection Author3 Heidi Hokka
* Hi, I'm Heidi! I'm 25 years old and I live in Sweden. I'm studying my second year of Computer Science at Mid Sweden University from a distance.
* This is my first team project so it's pretty exciting! I'm using CLion for this project.
*/

/**
 * @page general_information General information
 *
 * @section generalinfo General information
 * We're using a feature branch workflow for this project.
 * Our repository is hosted at Bitbucket, we use Trello(/w Kanban) and Slack for meetings and communication.
 *
 */