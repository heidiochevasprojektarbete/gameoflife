/**
 * @file test-generations.cpp
 * @author Heidi Hokka (first tests) and Eva Thilderkvist (additional tests)
 * @date October 2017
 * @version 0.1
*/

/**
 * These tests should be limited to testing the carry over of one generation to another.
 * Suggested testing-rule to use would be conway. Testing of rules themselves should be in another task.
 *
 * I suggest limiting the amount of generations and the size of the map so that testing is manageable.
 */

#include "catch.hpp"
#include "Cell_Culture/Population.h"
#include "Support/SupportStructures.h"
#include "Support/Globals.h"
#include "GameOfLife.h"

SCENARIO("Advancing a 0x0 Population for a generation"){
    GIVEN("A 0x0 Population"){
        // Set file name (0x0 population)
        fileName = "../test/seeds/Test_Gen_Seed_Pre00.txt";

        // Initiate test-population with conway
        Population cells00;
        cells00.initiatePopulation("conway");

        WHEN("Calculating a new generation"){
            int currentGen = cells00.calculateNewGeneration();

            THEN("The generation should have been incremented"){
                REQUIRE(currentGen > 0);
            }
        }
    }
}

SCENARIO("Advancing a single cell for a few generations"){
    GIVEN("A population with one live cell in 1x1 world"){
        // Set fileName for pre-population in Globals
        // File contains a lone cell in a 1x1 grid
        fileName = "../test/seeds/Test_Gen_Seed_Pre01.txt";

        // Initiate test-population with conway
        Population cells01;
        cells01.initiatePopulation("conway");

        THEN("It should not be dead since it was just created") {
            REQUIRE(cells01.getCellAtPosition({1, 1}).isAlive());
        }

        THEN("The action for the live cell should be DO_NOTHING (since it was just created in FileLoader (default value))"){
            REQUIRE(cells01.getCellAtPosition({1, 1}).getNextGenerationAction() == DO_NOTHING);
        }

        WHEN("Advancing a generation with the live cell") {
            int currentGen = cells01.calculateNewGeneration();

            THEN("The single live cell should still be alive (its first action should was DO_NOTHING)") {
                REQUIRE(cells01.getCellAtPosition({1, 1}).isAlive());
            }

            THEN("The rim cells should be unaffected"){
                // Code borrowed from Eva Thilderkvist from the population randomization tests!
                for(int row = 0; row <= WORLD_DIMENSIONS.HEIGHT +1; row++){
                    for(int column = 0; column <= WORLD_DIMENSIONS.WIDTH + 1; column++){

                        // If a cell is placed in a rim cell position
                        if((row == 0 || row == WORLD_DIMENSIONS.HEIGHT+1) || (column == 0 || column == WORLD_DIMENSIONS.WIDTH+1 )){
                            // It should be a rim cell
                            REQUIRE(cells01.getCellAtPosition({row, column}).isRimCell());

                            // It should not be alive
                            REQUIRE_FALSE(cells01.getCellAtPosition({row, column}).isAlive());

                            // It should have an age of 0 (dead)
                            REQUIRE(cells01.getCellAtPosition({row, column}).getAge() == 0);

                            // It should have the color black (dead)
                            REQUIRE(cells01.getCellAtPosition({row, column}).getColor() == COLOR::BLACK);

                            // It should have the default value #
                            REQUIRE(cells01.getCellAtPosition({row, column}).getCellValue() == '#');
                        }
                    }
                }
            }

            THEN("The next action for the live cell should be KILL_CELL") {
                REQUIRE(cells01.getCellAtPosition({1, 1}).getNextGenerationAction() == KILL_CELL);
            }

            THEN("The generation should be greater than 0 (default)") {
                REQUIRE(currentGen > 0);
            }

            WHEN("Advancing another generation with the live cell") {
                cells01.calculateNewGeneration();

                THEN("If rules have been executed it should now be dead") {
                    REQUIRE_FALSE(cells01.getCellAtPosition({1, 1}).isAlive());
                }

                WHEN("Advancing another ten generations") {
                    for (auto i = 0; i < 10; i++) {
                        currentGen = cells01.calculateNewGeneration();
                    }

                    THEN("The generation should be greater than 10") {
                        REQUIRE(currentGen > 10);
                    }

                    THEN("It should still be dead") {
                        REQUIRE_FALSE(cells01.getCellAtPosition({1, 1}).isAlive());
                    }

                    THEN("The rim cells should be unaffected"){
                        for(int row = 0; row <= WORLD_DIMENSIONS.HEIGHT +1; row++){
                            for(int column = 0; column <= WORLD_DIMENSIONS.WIDTH + 1; column++){

                                // If a cell is placed in a rim cell position
                                if((row == 0 || row == WORLD_DIMENSIONS.HEIGHT+1) || (column == 0 || column == WORLD_DIMENSIONS.WIDTH+1 )){
                                    // It should be a rim cell
                                    REQUIRE(cells01.getCellAtPosition({row, column}).isRimCell());

                                    // It should not be alive
                                    REQUIRE_FALSE(cells01.getCellAtPosition({row, column}).isAlive());

                                    // It should have an age of 0 (dead)
                                    REQUIRE(cells01.getCellAtPosition({row, column}).getAge() == 0);

                                    // It should have the color black (dead)
                                    REQUIRE(cells01.getCellAtPosition({row, column}).getColor() == COLOR::BLACK);

                                    // It should have the default value #
                                    REQUIRE(cells01.getCellAtPosition({row, column}).getCellValue() == '#');
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("Advancing a 2x2 Population with predictable pattern"){
    GIVEN("A pattern that never dies with 4 cells"){
        // Pattern (never dies):
        // xx --> xx --> xx
        // xx     xx     xx

        // Set file name
        fileName = "../test/seeds/Test_Gen_Seed_Pre02.txt";

        // Initiate test-population with conway
        Population cells02;
        cells02.initiatePopulation("conway");

        THEN("The amount of live cells should be 4"){
            int aliveCells = 0;

            for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++){
                for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++){
                    if(cells02.getCellAtPosition({row, column}).isAlive()){
                        aliveCells++;
                    }
                }
            }

            REQUIRE( aliveCells == 4);
        }

        THEN("The first cell should have the age of 1"){
            REQUIRE(cells02.getCellAtPosition({1, 1}).getAge() == 1);
        }

        WHEN("Advancing a generation"){
            int currentGen = cells02.calculateNewGeneration();

            THEN("The generation should be 1"){
                REQUIRE(currentGen == 1);
            }

            THEN("The first cell should have the age of 1"){
                REQUIRE(cells02.getCellAtPosition({1, 1}).getAge() == 1);
            }

            THEN("The amount of live cells should still be 4"){
                int aliveCells = 0;

                for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++){
                    for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++){
                        if(cells02.getCellAtPosition({row, column}).isAlive()){
                            aliveCells++;
                        }
                    }
                }

                REQUIRE( aliveCells == 4);
            }

            WHEN("Advancing ten generations more"){
                for (auto i = 0; i < 10; i++){
                    currentGen = cells02.calculateNewGeneration();
                }

                THEN("The first cell should have the age of 11"){
                    REQUIRE(cells02.getCellAtPosition({1, 1}).getAge() == 11);
                }

                THEN("The generation should be greater than 10"){
                    REQUIRE(currentGen > 10);
                }

                THEN("The 4 cells should still be alive"){
                    int aliveCells = 0;

                    for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++){
                        for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++){
                            if(cells02.getCellAtPosition({row, column}).isAlive()){
                                aliveCells++;
                            }
                        }
                    }

                    REQUIRE( aliveCells == 4);
                }

                THEN("The rim cells should be unaffected"){
                    for(int row = 0; row <= WORLD_DIMENSIONS.HEIGHT +1; row++){
                        for(int column = 0; column <= WORLD_DIMENSIONS.WIDTH + 1; column++){

                            // If a cell is placed in a rim cell position
                            if((row == 0 || row == WORLD_DIMENSIONS.HEIGHT+1) || (column == 0 || column == WORLD_DIMENSIONS.WIDTH+1 )){
                                // It should be a rim cell
                                REQUIRE(cells02.getCellAtPosition({row, column}).isRimCell());

                                // It should not be alive
                                REQUIRE_FALSE(cells02.getCellAtPosition({row, column}).isAlive());

                                // It should have an age of 0 (dead)
                                REQUIRE(cells02.getCellAtPosition({row, column}).getAge() == 0);

                                // It should have the color black (dead)
                                REQUIRE(cells02.getCellAtPosition({row, column}).getColor() == COLOR::BLACK);

                                // It should have the default value #
                                REQUIRE(cells02.getCellAtPosition({row, column}).getCellValue() == '#');
                            }
                        }
                    }
                }
            }
        }
    }
}

SCENARIO("Advancing a finite 3x3 Population with predictable pattern"){
    GIVEN("A finite pattern of 3x3") {
        // Pattern (dies eventually, x is alive, o is dead):
        // oox --> ooo --> ooo
        // oxo     oxo     ooo
        // xoo     ooo     ooo

        // Create the two first stages as Populations.
        fileName = "../test/seeds/Test_Gen_Seed_Pre03.txt";

        // Initiate first stage
        Population cells03Pre;
        cells03Pre.initiatePopulation("conway");

        // Change file
        fileName = "../test/seeds/Test_Gen_Seed_Post03.txt";

        // Initiate second stage
        Population cells03Post;
        cells03Post.initiatePopulation("conway");

        THEN("It should not have the same live cells compared to its next generation"){
            bool sameLiveCells = true;

            for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++){
                for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++){
                    Point point = {row, column};

                    if (cells03Pre.getCellAtPosition(point).isAlive() != cells03Post.getCellAtPosition(point).isAlive()){
                        sameLiveCells = false;
                    }
                }
            }

            REQUIRE_FALSE(sameLiveCells);
        }

        // Generation count starts at 1 after the population is initiated.
        WHEN("Calculating first generation"){
            int currentGen = cells03Pre.calculateNewGeneration();

            THEN("The generation should be 1"){
                REQUIRE(currentGen == 1);
            }

            THEN("The population should still have three cells alive"){
                int aliveCells = 0;

                for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++){
                    for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++){
                        if(cells03Pre.getCellAtPosition({row, column}).isAlive()){
                            aliveCells++;
                        }
                    }
                }

                REQUIRE( aliveCells == 3);
            }

            WHEN("Calculating the second generation"){
                currentGen = cells03Pre.calculateNewGeneration();

                THEN("The generation should have been incremented"){
                    REQUIRE(currentGen == 2);
                }

                THEN("The Population pattern should be equal to its next predicted generation"){
                    bool sameLiveCells = true;

                    for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++){
                        for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++){
                            Point point = {row, column};

                            if (cells03Pre.getCellAtPosition(point).isAlive() != cells03Post.getCellAtPosition(point).isAlive()){
                                sameLiveCells = false;
                            }
                        }
                    }

                    REQUIRE(sameLiveCells);
                }

                THEN("The rim cells should be unaffected"){
                    for(int row = 0; row <= WORLD_DIMENSIONS.HEIGHT +1; row++){
                        for(int column = 0; column <= WORLD_DIMENSIONS.WIDTH + 1; column++){

                            // If a cell is placed in a rim cell position
                            if((row == 0 || row == WORLD_DIMENSIONS.HEIGHT+1) || (column == 0 || column == WORLD_DIMENSIONS.WIDTH+1 )){
                                // It should be a rim cell
                                REQUIRE(cells03Pre.getCellAtPosition({row, column}).isRimCell());

                                // It should not be alive
                                REQUIRE_FALSE(cells03Pre.getCellAtPosition({row, column}).isAlive());

                                // It should have an age of 0 (dead)
                                REQUIRE(cells03Pre.getCellAtPosition({row, column}).getAge() == 0);

                                // It should have the color black (dead)
                                REQUIRE(cells03Pre.getCellAtPosition({row, column}).getColor() == COLOR::BLACK);

                                // It should have the default value #
                                REQUIRE(cells03Pre.getCellAtPosition({row, column}).getCellValue() == '#');
                            }
                        }
                    }
                }

                WHEN("Calculating the final generation"){
                    currentGen = cells03Pre.calculateNewGeneration();

                    THEN("No cells should be alive anymore"){
                        int aliveCells = 0;

                        for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++){
                            for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++){
                                if(cells03Pre.getCellAtPosition({row, column}).isAlive()){
                                    aliveCells++;
                                }
                            }
                        }

                        REQUIRE( aliveCells == 0);
                    }

                    THEN("The generation should have been incremented"){
                        REQUIRE(currentGen == 3);
                    }
                }
            }
        }
    }
}

SCENARIO("Advancing a population for a few generations with alternating rules") {
    GIVEN("A standard size random population initiated with two different rules") {
        WORLD_DIMENSIONS.WIDTH=80;
        WORLD_DIMENSIONS.HEIGHT=24;
        fileName="";
        Population twoGenRulesPop;
        twoGenRulesPop.initiatePopulation("conway", "erik");

        //Erik will show up on odd number generations. Generation 7 should show the effects on cells from "erik" because there
        //is living cells older than 5 (colour change is applied on 6-y-o's).
        WHEN("Population have aged 7 generations"){
            while(twoGenRulesPop.calculateNewGeneration() < 7);

            THEN("Erik should have been applied(odd number). Any 6-y-o should have changed colour"){
                //Look for 6-y-o's
               for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++) {
                    for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++) {
                        Cell& cell = twoGenRulesPop.getCellAtPosition(Point{column,row});
                        if(cell.getAge()== 6){
                            REQUIRE(twoGenRulesPop.getCellAtPosition(Point{column,row}).getColor() == STATE_COLORS.OLD);
                        }
                    }
                }
            }

            WHEN("Population have aged an additional generation (gen 8)"){
                twoGenRulesPop.calculateNewGeneration();

                THEN("Conway should have been applied. New 6-y-o's should not have changed colour yet"){
                    //Look for 6-y-o's
                    for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++) {
                        for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++) {
                            Cell& cell = twoGenRulesPop.getCellAtPosition(Point{column,row});
                            if(cell.getAge()== 6){
                                REQUIRE(twoGenRulesPop.getCellAtPosition(Point{column,row}).getColor() == STATE_COLORS.LIVING);
                            }
                        }
                    }
                }

                WHEN("Population have aged an additional generation (gen 9)"){
                    twoGenRulesPop.calculateNewGeneration();

                    THEN("Erik should be applied again. All 6-y-o's should have changed color again"){
                        //Look for 6-y-o's
                        for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++) {
                            for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++) {
                                Cell& cell = twoGenRulesPop.getCellAtPosition(Point{column,row});
                                if(cell.getAge()== 6){
                                    REQUIRE(twoGenRulesPop.getCellAtPosition(Point{column,row}).getColor() == STATE_COLORS.OLD);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

// This test will output with ScreenPrinter to the console, so don't get scared!
SCENARIO("Testing that the GameOfLife iterates through correct amount of generations"){
    GIVEN("A GameOfLife set to run 1 generation"){
        GameOfLife game(1, "conway", "conway");

        THEN("GameOfLife's current generation should be 0"){
            REQUIRE(game.getCurrentGeneration() == 0);
        }

        WHEN("After running the simulation"){
            game.runSimulation();

            THEN("GameOfLife's current generation should be 1"){
                REQUIRE(game.getCurrentGeneration() == 1);
            }
        }
    }

    GIVEN("A GameOfLife set to run 10 generations"){
        GameOfLife game(10, "conway", "conway");

        THEN("GameOfLife's current generation should be 0"){
            REQUIRE(game.getCurrentGeneration() == 0);
        }

        WHEN("After running the simulation"){
            game.runSimulation();

            THEN("GameOfLife's current generation should be 10"){
                REQUIRE(game.getCurrentGeneration() == 10);
            }
        }
    }
}


