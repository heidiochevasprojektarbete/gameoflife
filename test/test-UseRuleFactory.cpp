/**
 * @file test-UseRuleFactory.cpp
 * @author Eva Thilderkvist (first tests) and Heidi Hokka (additional tests)
 * @date October 2017
 * @version 0.1
*/

#include <catch.hpp>
#include "../include/Cell_Culture/Population.h"

SCENARIO("Requesting one rule only"){
    GIVEN("A Population initiated with the valid string 'conway'"){
        Population testPop;
        testPop.initiatePopulation("conway");

        THEN("The the even rule should be set to conway"){
            REQUIRE(testPop.getEvenRuleName() == "conway");
        }
        THEN("The odd rule should also be set to conway"){
            REQUIRE(testPop.getOddRuleName() == "conway");
        }
    }

    GIVEN("A Population initiated with the invalid string 'chess'"){
        Population testPop;
        testPop.initiatePopulation("chess");

        THEN("The even rule should be defaulted to 'conway'"){
            REQUIRE(testPop.getEvenRuleName() == "conway");
        }
        THEN("The odd rule should be defaulted to 'conway'"){
            REQUIRE(testPop.getOddRuleName() == "conway");
        }
    }
}

SCENARIO("Requesting two different rules"){
    GIVEN("A Population initiated with the two valid strings"){
        Population testPop;
        testPop.initiatePopulation("erik","von_neumann");

        THEN("The even rule should be created as an 'erik' rule"){
            REQUIRE(testPop.getEvenRuleName() == "erik");
        }
        THEN("The odd rule should be created as a 'von_neumann' rule"){
            REQUIRE(testPop.getOddRuleName() == "von_neumann");
        }
    }

    GIVEN("A population initiated with a valid even rule and an invalid odd rule"){
        Population testPop;
        testPop.initiatePopulation("erik","chess");

        THEN("The even rule should be created as an 'erik' rule"){
            REQUIRE(testPop.getEvenRuleName() == "erik");
        }
        THEN("The odd rule should be created as a default 'conway' rule"){
            REQUIRE(testPop.getOddRuleName() == "conway");
        }
    }

    GIVEN("A population initiated with an invalid even rule and a valid pdd rule"){
        Population testPop;
        testPop.initiatePopulation("chess","erik");

        THEN("The even rule should be created as a default 'conway' rule"){
            REQUIRE(testPop.getEvenRuleName() == "conway");
        }
        THEN("The odd rule should be created as an 'erik' rule"){
            REQUIRE(testPop.getOddRuleName() == "erik");
        }
    }

    GIVEN("A population with two invalid string"){
        Population testPop;
        testPop.initiatePopulation("chess","poker");

        THEN("The even rule should be created as a default 'conway' rule"){
            REQUIRE(testPop.getEvenRuleName() == "conway");
        }
        THEN("The odd rule should be created as a default 'conway' rule"){
            REQUIRE(testPop.getOddRuleName() == "conway");
        }
    }
}
