/**
 * @file test-TestCell.cpp
 * @author Eva Thilderkvist (first tests) and Heidi Hokka (additional tests)
 * @date October 2017
 * @version 0.1
*/

#include "catch.hpp"
#include "Cell_Culture/Cell.h"

SCENARIO("Performing actions on and retrieving information from a cell"){
    GIVEN("An normal cell with DO_NOTHING action"){
        Cell doNCell(false, DO_NOTHING);

        THEN("Cell should be dead"){
            REQUIRE_FALSE(doNCell.isAlive());
        }
        THEN("It should have a zero age"){
            REQUIRE(doNCell.getAge()== 0);
        }
        THEN("Cell should have 'dead' color"){
            REQUIRE(doNCell.getColor() == STATE_COLORS.DEAD);
        }
        THEN("Next generation action should be 'do nothing'"){
            REQUIRE(doNCell.getNextGenerationAction() == DO_NOTHING);
        }
        THEN("Cell value should be '#'"){
            REQUIRE(doNCell.getCellValue()=='#');
        }
        THEN("Cell is not expected to be alive next generation"){
            REQUIRE_FALSE(doNCell.isAliveNext());
        }
        THEN("Cell is not a rim cell"){
            REQUIRE_FALSE(doNCell.isRimCell());
        }
        WHEN("Cell is updated"){
            doNCell.updateState();

            THEN("Cell Should still be dead"){
                REQUIRE_FALSE(doNCell.isAlive());
            }
            THEN("Cell shouldn't have aged"){
                REQUIRE(doNCell.getAge()== 0);
            }
            THEN("Cell shouldn't have changed color"){
                REQUIRE(doNCell.getColor() == STATE_COLORS.DEAD);
            }
            THEN("Next generation action should be 'do nothing'"){
                REQUIRE(doNCell.getNextGenerationAction() == DO_NOTHING);
            }
            THEN("Cell shouldn't have changed value"){
                REQUIRE(doNCell.getCellValue()=='#');
            }
            THEN("Cell is still not expected to be alive next generation"){
                REQUIRE_FALSE(doNCell.isAliveNext());
            }
            THEN("The Cell should not have changed to a rim cell"){
                REQUIRE_FALSE(doNCell.isRimCell());
            }
        }
        WHEN("Next update details is set"){
            doNCell.setNextCellValue('T');
            doNCell.setNextColor(STATE_COLORS.ELDER);

            WHEN("Cell is updated"){
                doNCell.updateState();

                THEN("Cell should have changed color"){
                    REQUIRE(doNCell.getColor() == STATE_COLORS.ELDER);
                }
                THEN("Cell should have changed value"){
                    REQUIRE(doNCell.getCellValue() == 'T');
                }
            }

            WHEN("Cell is set to be given life"){
                doNCell.setNextGenerationAction(GIVE_CELL_LIFE);
                doNCell.setIsAliveNext(true);

                THEN("Next generation action should be 'give life'"){
                    REQUIRE(doNCell.getNextGenerationAction() == GIVE_CELL_LIFE);
                }
                THEN("Cell is expected to be alive next generation"){
                    REQUIRE(doNCell.isAliveNext());
                }

                WHEN("Cell is updated"){
                    doNCell.updateState();

                    THEN("Cell should be alive"){
                        REQUIRE(doNCell.isAlive());
                    }
                    THEN("Next generation action should be set to do nothing"){
                        REQUIRE(doNCell.getNextGenerationAction() == DO_NOTHING);
                    }
                    THEN("Cell should have aged a year"){
                        REQUIRE(doNCell.getAge() == 1);
                    }

                    WHEN("Cell is aged"){
                        doNCell.setNextGenerationAction(IGNORE_CELL);
                        doNCell.updateState();

                        THEN("Cell should have incremented age"){
                            REQUIRE(doNCell.getAge()==2);
                        }
                        THEN("Next generation action should be set to do nothing"){
                            REQUIRE(doNCell.getNextGenerationAction() == DO_NOTHING);
                        }

                        WHEN("Cell is killed"){
                            doNCell.setNextGenerationAction(KILL_CELL);
                            doNCell.updateState();

                            THEN("Cell should be dead"){
                                REQUIRE_FALSE(doNCell.isAlive());
                            }
                            THEN("Cell should have an age of 0"){
                                REQUIRE(doNCell.getAge() == 0);
                            }
                            THEN("Next generation action should be set to do nothing"){
                                REQUIRE(doNCell.getNextGenerationAction() == DO_NOTHING);
                            }
                            THEN("The Cell should not have changed to a rim cell"){
                                REQUIRE_FALSE(doNCell.isRimCell());
                            }
                        }
                    }
                }
            }
        }
    }
    GIVEN("A cell to be set alive"){
        Cell aliveCell(false,GIVE_CELL_LIFE);

        THEN("Cell should be alive"){
            REQUIRE(aliveCell.isAlive());
        }
        THEN("Cell should have been given 'alive' color"){
            REQUIRE(aliveCell.getColor() == STATE_COLORS.LIVING);
        }

        WHEN("Cell is updated"){
            aliveCell.updateState();

            THEN("Cell should not have aged"){
                REQUIRE(aliveCell.getAge()==1);
            }
            THEN("Cell should still have 'alive' color"){
                REQUIRE(aliveCell.getColor()==STATE_COLORS.LIVING);
            }
        }
    }
    GIVEN("A plain rim cell"){
        Cell rimCell(true);

        THEN("Cell is a rim cell"){
            REQUIRE(rimCell.isRimCell());
        }
        THEN("Cell should be dead"){
            REQUIRE_FALSE(rimCell.isAlive());
        }
        THEN("Next action should be to do nothing"){
            REQUIRE(rimCell.getNextGenerationAction() == DO_NOTHING);
        }
        WHEN("Trying to give cell life"){
            rimCell.setNextGenerationAction(GIVE_CELL_LIFE);
            rimCell.updateState();

            THEN("Cell should still be dead"){
                REQUIRE_FALSE(rimCell.isAlive());
            }
            THEN("Cell shouldn't have aged"){
                REQUIRE(rimCell.getAge()==0);
            }
        }
        WHEN("Trying to kill cell"){
            rimCell.setNextGenerationAction(KILL_CELL);
            rimCell.updateState();

            THEN("Cell is still dead"){
                REQUIRE_FALSE(rimCell.isAlive());
            }
        }
        WHEN("Trying to change cell colour and value"){
            rimCell.setNextColor(STATE_COLORS.LIVING);
            rimCell.setNextCellValue('F');
            rimCell.updateState();

            //FAILS, bug! colour shouldn't be able to be changed and updated on rimcell
            THEN("Colour shouldn't be able to change"){
                REQUIRE(rimCell.getColor() == STATE_COLORS.DEAD);
            }
            //FAILS, bug! colour shouldn't be able to be changed and updated on rimcell
            THEN("Cell value shouldn't be able to change"){
                REQUIRE(rimCell.getCellValue()=='#');
            }

            THEN("The Cell should still be a rim cell"){
                REQUIRE(rimCell.isRimCell());
            }
        }
    }
}