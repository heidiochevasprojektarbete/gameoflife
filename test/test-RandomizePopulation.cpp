/**
 * @file test-RandomizePopulation.cpp
 * @author Eva Thilderkvist (first tests) and Heidi Hokka (additional tests)
 * @date October 2017
 * @version 0.1
*/

#include "catch.hpp"
#include "../include/Cell_Culture/Population.h"
#include <fstream>
#include <iostream>

/**
 * Tests for the construction of a cellpopulation where the state of the cells are randomly generated.
 * Tests is limited to testing what is happening between GameOfLife trying to initiate a population
 * until the population is expected to be read.
 * Tests relating to dealing with the RuleFactory is a separate testing task.
*/

SCENARIO("No filename exists"){
    fileName = "";
    WORLD_DIMENSIONS.HEIGHT = 24;   //using default dimensions
    WORLD_DIMENSIONS.WIDTH = 80;

    GIVEN("An uninitialized population"){
        Population testPop;

        THEN("The population shouldn't contain any cells"){
            REQUIRE(testPop.getTotalCellPopulation() == 0);
        }

        WHEN("The popultion is initiated"){
            testPop.initiatePopulation("conway");

            THEN("The population should contain the correct number of cells"){
                REQUIRE(testPop.getTotalCellPopulation() == 2132); //(Default dimensions of 80 x 24 + rim cells = 2132)
            }

            THEN("Border of population should contain rimCells"){

                for(int rowNum = 0; rowNum <= WORLD_DIMENSIONS.HEIGHT +1; rowNum++){    //Check row by row
                    for(int colNum = 0; colNum <= WORLD_DIMENSIONS.WIDTH + 1; colNum++){    //Check column by column

                        //If cell is in first or last row it should be a rimCell
                        if(rowNum == 0 || rowNum == WORLD_DIMENSIONS.HEIGHT+1){
                           REQUIRE(testPop.getCellAtPosition(Point{colNum,rowNum}).isRimCell());
                        }

                        //If cell is in first or last column it should be a rimCell
                        if(colNum == 0 || colNum == WORLD_DIMENSIONS.WIDTH +1){
                            REQUIRE(testPop.getCellAtPosition(Point{colNum,rowNum}).isRimCell());
                        }
                    }
                }
            }

            THEN("Population should have a different value every time it gets randomized"){
                //read last randomization
                string lastRandom;
                ifstream inFile("../test/seeds/LastRandom.txt");
                getline (inFile,lastRandom);
                inFile.close();

                //Read current randomization map
                ostringstream curStream;
                for(int rowNum = 0; rowNum <= WORLD_DIMENSIONS.HEIGHT +1; rowNum++) {    //Check row by row
                    for (int colNum = 0; colNum <= WORLD_DIMENSIONS.WIDTH + 1; colNum++) {    //Check column by column
                        curStream << testPop.getCellAtPosition(Point{colNum,rowNum}).isAlive();
                    }
                }

                //save it
                string currentRandom = curStream.str();
                ofstream outFile("../test/seeds/LastRandom.txt");
                outFile << curStream.str();
                outFile.close();

                //Compare the seeds
                bool isIdentical = true;
                for(int seedIdx = 0; seedIdx < currentRandom.length(); seedIdx++){
                    if(currentRandom.at(seedIdx) != lastRandom.at(seedIdx)){
                        isIdentical=false;
                        break;
                    }
                }

                REQUIRE_FALSE(isIdentical);
            }
        }
    }
}
