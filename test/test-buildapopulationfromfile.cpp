/**
 * @file test-buildapopulationfromfile.cpp
 * @author Heidi Hokka (initial tests) and Eva Thilderkvist (additional)
 * @date October 2017
 * @version 0.1
*/

#include "Catch.hpp"
#include "Cell_Culture/Population.h"
#include "Support/FileLoader.h"
#include "Support/Globals.h"

/**
 * Develop tests for building a cell population where the specifics is loaded from a given file. Test should be limited
 * to testing what is happening between GameOfLife trying to initiate a population until the population is expected
 * to be read. Anything relating to dealing with the RuleFactory should be a separate testing task.
 *
 * Develop tests dealing with building a population from a file. The randomization alternative is another test-scenario.
 */

SCENARIO("No fileName is set"){
    // Reset fileName and WORLD_DIMENSIONS to defaults if other tests are run.
    fileName = "";
    WORLD_DIMENSIONS.HEIGHT = 24;
    WORLD_DIMENSIONS.WIDTH = 80;

    GIVEN("A new uninitiated Population"){
        Population cells;

        THEN("The cells should not have been initiated"){
            REQUIRE_FALSE(cells.getTotalCellPopulation()); // false = 0
        }

        WHEN("The Population is initiated"){
            cells.initiatePopulation("conway");

            THEN("fileName should not be set"){
                REQUIRE(fileName == "");
            }

            THEN("Cells should now exist due to randomization (Default dimensions of 80 x 24 + rim cells = 2132)"){
                REQUIRE(cells.getTotalCellPopulation() == 2132);
            }

            THEN("The WORLD_DIMENSIONS should not have changed"){
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            }
        }
    }
}

SCENARIO("A filename is set"){
    GIVEN("A valid file path"){
        // Default seed file for project
        fileName = "../Population_Seed.txt";
        //assuming the previous file  (Point to cell in map, isAlive?)
        map<Point, bool> controlCells={{Point{6,3},false},{Point{9,5},false},{Point{12,5},false},{Point{16,9},false},{Point{18,8},false},
                                       {Point{7,2},true},{Point{13,3},true},{Point{2,4},true},{Point{17,9},true},{Point{18,9},true}};

        // Reset WORLD_DIMENSIONS
        WORLD_DIMENSIONS.WIDTH = 80;
        WORLD_DIMENSIONS.HEIGHT = 24;

        GIVEN("A new uninitiated Population"){
            Population cells;

            THEN("The cells should not have been initiated"){
                REQUIRE_FALSE(cells.getTotalCellPopulation()); // false = 0
            }

            WHEN("The Population is initiated"){
                cells.initiatePopulation("conway");

                THEN("fileName should be set"){
                    REQUIRE_FALSE(fileName == "");
                }

                THEN("Cells should now exist due to being loaded from the default file (20 x 10 + rim cells = 264)"){
                    REQUIRE(cells.getTotalCellPopulation() == 264);
                }

                THEN("The WORLD_DIMENSIONS should have changed to 20 x 10"){
                    REQUIRE(WORLD_DIMENSIONS.WIDTH == 20);
                    REQUIRE(WORLD_DIMENSIONS.HEIGHT == 10);
                }

                THEN("The cell population should have been built according to file specifications"){
                    //loop control map
                    for ( const auto &cont : controlCells )
                    {
                        REQUIRE(cells.getCellAtPosition(cont.first).isAlive() == cont.second);
                    }
                }
            }
        }
    }
}

SCENARIO("A fileName is set, but the file doesn't exist"){
    // Missing path to file, since the file is one folder above GameOfLife-Test.exe
    fileName = "Population_Seed.txt";

    // Reset WORLD_DIMENSIONS
    WORLD_DIMENSIONS.WIDTH = 80;
    WORLD_DIMENSIONS.HEIGHT = 24;

    GIVEN("A new uninitiated Population"){
        Population cells;

        THEN("The cells should not have been initiated"){
            REQUIRE_FALSE(cells.getTotalCellPopulation()); // false = 0
        }

        WHEN("The Population is initiated with the nonexistent file"){
            REQUIRE_THROWS(cells.initiatePopulation("conway"));

            THEN("fileName should still be set"){
                REQUIRE_FALSE(fileName == "");
            }

            THEN("Cells should not exist due to failing to load the nonexistent file"){
                REQUIRE_FALSE(cells.getTotalCellPopulation()); // false = 0
            }

            THEN("The WORLD_DIMENSIONS should not have changed"){
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            }
        }
    }
}

SCENARIO("A valid fileName is set, but the file has an invalid seed"){
    // The first character in the seed file (derived from the default) is a 2 after reading the dimensions.
    fileName = "../test/seeds/invalid_seed.txt";

    // Reset WORLD_DIMENSIONS
    WORLD_DIMENSIONS.WIDTH = 80;
    WORLD_DIMENSIONS.HEIGHT = 24;

    GIVEN("A new uninitiated Population"){
        Population cells;

        THEN("The cells should not have been initiated"){
            REQUIRE_FALSE(cells.getTotalCellPopulation()); // false = 0
        }

        WHEN("The Population is initiated with the invalid seed"){

            cells.initiatePopulation("conway");

            THEN("fileName should still be set"){
                REQUIRE_FALSE(fileName == "");
            }

            // The valid characters in the file are loaded - but invalid characters
            // are simply skipped leading to less cells in the end. (Result: 263, should be
            // 264 when loading from normal file without the '2' as the first character)

            // FAILS
            THEN("Cells should not exist due to failing to load the invalid seed"){
                REQUIRE_FALSE(cells.getTotalCellPopulation()); // false = 0
            }

            // FAILS
            // The world dimensions are set from the file
            THEN("The WORLD_DIMENSIONS should not have changed"){
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            }
        }
    }
}

SCENARIO("A valid fileName is set, but the file has invalid starchy content"){
    // The file contains words and some zeroes and ones.
    fileName = "../test/seeds/invalid_content.txt";

    // Reset WORLD_DIMENSIONS
    WORLD_DIMENSIONS.WIDTH = 80;
    WORLD_DIMENSIONS.HEIGHT = 24;

    GIVEN("A new uninitiated Population"){
        Population cells;

        THEN("The cells should not have been initiated"){
            REQUIRE_FALSE(cells.getTotalCellPopulation()); // false = 0
        }

        WHEN("The Population is initiated with the invalid content"){

            cells.initiatePopulation("conway");

            THEN("fileName should still be set"){
                REQUIRE_FALSE(fileName == "");
            }

            // The result of words, zeroes and ones is apparently 74 cells.
            // I am unsure why this happens. The amount of characters entered
            // in the file (minus any invisible characters) is 35.
            // To this I haven't added rim cells.

            // FAILS
            THEN("Cells should not exist due to failing to load the invalid seed"){
                REQUIRE_FALSE(cells.getTotalCellPopulation()); // false = 0
            }

            // FAILS
            // The world dimensions are set from the file
            THEN("The WORLD_DIMENSIONS should not have changed"){
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
            }
        }
    }
}