/**
 * @file test-main.cpp
 * @author Eva Thilderkvist, Heidi Hokka
 * @date November 2017
 * @version 1.0
*/

// File where Catch takes over the main() to run tests.
#define CATCH_CONFIG_MAIN
#include "catch.hpp"