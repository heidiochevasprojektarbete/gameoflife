/**
 * @file test-initapplication.cpp
 * @author Heidi Hokka (first tests) and Eva Thilderkvist (additional tests)
 * @date October 2017
 * @version 0.1
*/

#include "catch.hpp"
#include "Support/MainArgumentsParser.h"
#include "Support/MainArguments.h"
#include "Support/Globals.h"
#include "memstat.hpp"

/**
* From task description:
* "Develop tests for everything that has to do with getting the program up and running.
* This task is limited to program flow before any simulation is actually started."
*
* "There is specific and limited events which can occur within the confinement of this test-area.
* Classes involved with the assessment will be **BaseArgument**, it's derived classes as well as
* the **ArgumentParser**. Some global aspects of the application which is used should also be included in the tests if needed.
*
* One of the tests causes a function to leak, that is why I've included memstat. It leaks for each argument that
* is created with new (which isn't deleted if an argument throws an exception!).
*
* Notes:
* I have commented above the tests fail with FAILS. (I've tested all of them with a failing test first, but these do not
* stop failing after writing a "correct" test.)
*/

SCENARIO("ApplicationValues is created"){
    ApplicationValues app;

    THEN("The runSimulation bool in ApplicationValues should be true (default)"){
        REQUIRE(app.runSimulation);
    }

    THEN("The even rule string in ApplicationValues should be empty prior to parsing (default)"){
        REQUIRE(app.evenRuleName == "");
    }

    THEN("The odd rule string in ApplicationValues should be empty prior to parsing (default)"){
        REQUIRE(app.oddRuleName == "");
    }

    THEN("The default value for maxGenerations in ApplicationValues should be 100 (default)"){
        REQUIRE(app.maxGenerations == 100);
    }
}

SCENARIO("User starts program with no valid arguments"){
    MainArgumentsParser parser;

    GIVEN("No arguments to the parser - just running the program"){
        // The first argument is always the program name
        char* argArr[] = {(char *)"GameOfLife"};
        int argCount = 1;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The runSimulation bool in ApplicationValues should be true (default)"){
            REQUIRE(app.runSimulation);
        }

        THEN("The even rule string in ApplicationValues should be conway after parsing (default)"){
            REQUIRE(app.evenRuleName == "conway");
        }

        THEN("The odd rule string in ApplicationValues should be conway after parsing (default)"){
            REQUIRE(app.oddRuleName == "conway");
        }

        THEN("The default value for maxGenerations in ApplicationValues should be 100 (default)"){
            REQUIRE(app.maxGenerations == 100);
        }

        THEN("The fileName global variable should be empty"){
            REQUIRE(fileName == "");
        }

        THEN("The WORLD_DIMENSIONS global variable should be the default {80, 24}"){
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
        }
    }

    GIVEN("Invalid arguments to the parser"){
        char* argArr[] = {(char *)"GameOfLife",(char *)"potato", (char *)"123", (char *)"helloworld", (char *)"asd123", (char *)"password"};
        int argCount = 6;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The runSimulation bool in ApplicationValues should be true (default)"){
            REQUIRE(app.runSimulation);
        }

        THEN("The even rule string in ApplicationValues should be conway after parsing (default)"){
            REQUIRE(app.evenRuleName == "conway");
        }

        THEN("The odd rule string in ApplicationValues should be conway after parsing (default)"){
            REQUIRE(app.oddRuleName == "conway");
        }

        THEN("The value for maxGenerations in ApplicationValues should be 100 (default)"){
            REQUIRE(app.maxGenerations == 100);
        }

        THEN("The fileName global variable should be empty"){
            REQUIRE(fileName == "");
        }

        THEN("The WORLD_DIMENSIONS global variable should be the default {80, 24}"){
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
        }
    }
}

SCENARIO("User starts program with valid arguments"){
    MainArgumentsParser parser;

    GIVEN("'-h' as an argument"){
        char* argArr[] = {(char*)"GameOfLife",(char *)"-h"};
        int argCount = 2;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The runSimulation bool in ApplicationValues should be false"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }

    GIVEN("'-g' and '200' as arguments"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-g", (char *)"200"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The value for maxGenerations in ApplicationValues should be 200"){
            REQUIRE(app.maxGenerations == 200);
        }

        THEN("The runSimulation bool should not have changed from default (true)"){
            REQUIRE(app.runSimulation);
        }

        THEN("The even rule string in ApplicationValues should be conway (default)"){
            REQUIRE(app.evenRuleName == "conway");
        }

        THEN("The odd rule string in ApplicationValues should be conway (default)"){
            REQUIRE(app.oddRuleName == "conway");
        }
    }


    GIVEN("'-s' and '24x24' as arguments"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-s", (char *)"24x24"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The WORLD_DIMENSIONS global variable should have width and height of 24"){
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 24);

            // Reset global variable WORLD_DIMENSIONS
            WORLD_DIMENSIONS.HEIGHT = 24;
            WORLD_DIMENSIONS.WIDTH = 80;
        }
    }

    GIVEN("'-f' and 'cells.txt' as arguments"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-f", (char *)"cells.txt"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The fileName global variable should contain 'cells.txt'"){
            REQUIRE(fileName == "cells.txt");
            fileName = ""; //Reset, because it is a global variable.
        }
    }

    GIVEN("'-er' and 'erik'"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-er", (char *)"erik"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The even rule string should be erik"){
            REQUIRE(app.evenRuleName == "erik");
        }

        THEN("The odd rule string should be erik (same as even)"){
            REQUIRE(app.oddRuleName == "erik");
        }
    }

    GIVEN("'-or' and 'von_neumann'"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-or", (char *)"von_neumann"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The even rule string should be conway (default if none)"){
            REQUIRE(app.evenRuleName == "conway");
        }

        THEN("The odd rule string should be von_neumann"){
            REQUIRE(app.oddRuleName == "von_neumann");
        }
    }

    GIVEN("'-er', 'von_neumann', '-or' and 'erik'"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-er", (char *)"von_neumann", (char *)"-or", (char *)"erik"};
        int argCount = 5;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The even rule string should be von_neumann"){
            REQUIRE(app.evenRuleName == "von_neumann");
        }

        THEN("The odd rule string should be erik"){
            REQUIRE(app.oddRuleName == "erik");
        }
    }
}

SCENARIO("User gives arguments without value"){
    MainArgumentsParser parser;

    GIVEN("'-g' as an argument, without value"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-g"};
        int argCount = 2;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The value for maxGenerations in ApplicationValues should not have changed (100)"){
            REQUIRE(app.maxGenerations == 100);
        }

        THEN("The simulation should not be run"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }

    GIVEN("'-s' as an argument, without value"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-s"};
        int argCount = 2;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The WORLD_DIMENSIONS global variable should not have changed {80, 24}"){
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
        }

        THEN("The simulation should not be run"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }

    GIVEN("'-f' as an argument, without value"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-f"};
        int argCount = 2;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The fileName global variable should not have changed (be empty)"){
            REQUIRE(fileName == "");
        }

        THEN("The simulation should not be run"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }

    GIVEN("'-or' as an argument, without value"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-or"};
        int argCount = 2;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The odd rule should be default (conway)"){
            REQUIRE(app.oddRuleName == "conway");
        }

        THEN("The simulation should not be run"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }

    GIVEN("'-er' as an argument, without value"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-er"};
        int argCount = 2;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The even rule should be default (conway)"){
            REQUIRE(app.oddRuleName == "conway");
        }

        THEN("The simulation should not be run"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }
}

// These tests leak due to GenerationArgument throwing before
// the "new"-allocated Arguments are "delete":d in runParser.

SCENARIO("User gives invalid values to parser"){

     MainArgumentsParser parser;

   GIVEN("'-g', '-s', as arguments, without their required values"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-g", (char *)"-s"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("Require the function to throw, since it tries to convert the second argument to int (GenerationsArgument::execute())"){
            REQUIRE_THROWS(parser.runParser(argArr, argCount));
        }
    }

    GIVEN("'-g' and 'potato' as arguments"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-g", (char *)"potato"};
        int argCount = 3;

        THEN("Require the function to throw, since it tries to convert the second argument to int (GenerationsArgument::execute())"){
            REQUIRE_THROWS(parser.runParser(argArr, argCount));
        }
    }
}


SCENARIO("User gives same parameter several times"){
    MainArgumentsParser parser;

    GIVEN("The argument '-h' three times"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-h", (char *)"-h", (char *)"-h"};
        int argCount = 4;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The runSimulation bool in ApplicationValues should be false"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }

    GIVEN("The argument '-g' with a value two times"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-g", (char *)"200", (char *)"-g", (char *)"300"};
        int argCount = 5;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("Only the first value should be read (200)"){
            REQUIRE(app.maxGenerations == 200);
        }
    }
}

SCENARIO("User gives invalid world dimensions"){
    MainArgumentsParser parser;

    GIVEN("The argument '-s' with '20x20x20'"){

        char* argArr[] = {(char*)"GameOfLife", (char *)"-s", (char *)"20x20x20"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("Only 20x20 should be read to dimensions"){
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 20);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 20);

            // Reset global variable WORLD_DIMENSIONS
            WORLD_DIMENSIONS.HEIGHT = 24;
            WORLD_DIMENSIONS.WIDTH = 80;
        }
    }

    GIVEN("The argument '-s' with '0x0'"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-s", (char *)"0x0"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("0x0 should be read to dimensions"){
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 0);
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 0);

            // Reset global variable WORLD_DIMENSIONS
            WORLD_DIMENSIONS.HEIGHT = 24;
            WORLD_DIMENSIONS.WIDTH = 80;
        }
    }

    // FAILS
    GIVEN("The argument '-s' with '10', 'x', '10'"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-s", (char *)"10", (char *)"x", (char *)"10"};
        int argCount = 5;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The values of HEIGHT and WIDTH should not have changed") {
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

            // Reset global variable WORLD_DIMENSIONS
            WORLD_DIMENSIONS.HEIGHT = 24;
            WORLD_DIMENSIONS.WIDTH = 80;
        }

        THEN("The WIDTH should not have changed"){
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);

            // Reset global variable WORLD_DIMENSIONS
            WORLD_DIMENSIONS.HEIGHT = 24;
            WORLD_DIMENSIONS.WIDTH = 80;
        }

        THEN("Simulation should not be run"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }

    // FAILS
    GIVEN("The argument '-s' with '100xx100'"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-s", (char *)"100xx100"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The values of HEIGHT should not have changed") {
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

            // Reset global variable WORLD_DIMENSIONS
            WORLD_DIMENSIONS.HEIGHT = 24;
            WORLD_DIMENSIONS.WIDTH = 80;
        }

        THEN("The WIDTH should not have changed"){
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);

            // Reset global variable WORLD_DIMENSIONS
            WORLD_DIMENSIONS.HEIGHT = 24;
            WORLD_DIMENSIONS.WIDTH = 80;
        }

        THEN("Simulation should not be run"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }

    // FAILS
    GIVEN("The argument '-s' with 'potato'"){
        char* argArr[] = {(char*)"GameOfLife", (char *)"-s", (char *)"potato"};
        int argCount = 3;
        ApplicationValues app = parser.runParser(argArr, argCount);

        THEN("The value of HEIGHT should not have changed") {
            REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);

            // Reset global variable WORLD_DIMENSIONS
            WORLD_DIMENSIONS.HEIGHT = 24;
            WORLD_DIMENSIONS.WIDTH = 80;
        }

        THEN("The WIDTH should not have changed"){
            REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);

            // Reset global variable WORLD_DIMENSIONS
            WORLD_DIMENSIONS.HEIGHT = 24;
            WORLD_DIMENSIONS.WIDTH = 80;
        }

        THEN("Simulation should not be run"){
            REQUIRE_FALSE(app.runSimulation);
        }
    }
}

