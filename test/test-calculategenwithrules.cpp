/**
* @file test-calculategenwithrules.cpp
* @author Heidi Hokka (initial tests) and Eva Thilderkvist (additional tests)
* @date November 2017
* @version 0.1
*/

#include "catch.hpp"
#include "Cell_Culture/Cell.h"
#include "Cell_Culture/Population.h"
#include "Support/Globals.h"
#include "Support/SupportStructures.h"

/*
 * Develop tests for a generation with the conway, von_neumann and erik  rulesets.
 * Are the rules followed as stated in documentation?
 *
 * For these tests, following seed files have been created:
 * rules_overpopulation_seed_alldir.txt
 * rules_overpopulation_seed_cardinaldir.txt
 * rules_underpopulation_seed_alldir.txt
 * rules_underpopulation_seed_cardinaldir.txt
 * rules_resurrection_seed_alldir.txt
 * rules_resurrection_seed_cardinaldir.txt
 * rules_survives_seed_alldir.txt
 * rules_survives_seed_cardinaldir.txt
 *
 * All files contain a seed of size 3x3 and the cell in the middle (at position 2,2) is analyzed in the tests.
 *
 * alldir means that the cell in the middle will die/be resurrected if all directions are checked.
 * cardinaldir means that the cell in the middle will die/be resurrected if ONLY the cardinal directions are checked.
 *
 * overpopulation is for seeds that cause death for the cell in the center due to having too many neighbours
 * underpopulation is for seeds that cause death for the cell in the center due to having too few neighbours
 * resurrection is for seeds that give life to the dead cell in the center
 * survives means that the live cell has 2 to 3 neighbours which means that the cell stays alive
 *
 * Since all rules follow the underpopulation = < 2, overpopulation = > 3 and resurrection = = 3 rules,
 * this will be the basis for the file content.
 *
 * It will be necessary to check the contents of the files to get an understanding of what is about to occur.
 */

SCENARIO("Testing the Conway rules"){
    GIVEN("A seed where a live cell in the middle has less than two neighbours in all directions"){
        fileName = "../test/seeds/rules_underpopulation_seed_alldir.txt";
        Population population;
        population.initiatePopulation("conway");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        // The action to KILL_CELL is set after the first calculation,
        // that's why two generations are required for test
        WHEN("Advancing two generations with the Conway rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should now be dead"){
                REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where a live cell in the middle has more than three neighbours in all directions"){
        fileName = "../test/seeds/rules_overpopulation_seed_alldir.txt";
        Population population;
        population.initiatePopulation("conway");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the conway rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should now be dead"){
                REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where a dead cell in the middle has exactly three neighbours (checking all directions)"){
        fileName = "../test/seeds/rules_resurrection_seed_alldir.txt";
        Population population;
        population.initiatePopulation("conway");

        THEN("The cell in the center should be dead"){
            REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the conway rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The cell should now be alive"){
                REQUIRE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where a live cell has enough neighbours to survive (checking all directions)"){
        fileName = "../test/seeds/rules_survives_seed_alldir.txt";
        Population population;
        population.initiatePopulation("conway");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the conway rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The cell should still be alive thanks to its neighbours"){
                REQUIRE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }
}

// von Neumann differ to Conway in that only the cardinal (north, east, south, west) directions are checked
// Tests are done with files that contain seeds for both all directions, and cardinal directions.
// If cells are placed outside the cardinal directions, then the von Neumann rule should ignore those cells.
SCENARIO("Testing the von Neumann rules"){
    GIVEN("A seed where overpopulation (> 3 neighbours) would occur in CARDINAL directions for cell in middle"){
        fileName = "../test/seeds/rules_overpopulation_seed_cardinaldir.txt";
        Population population;
        population.initiatePopulation("von_neumann");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Von Neumann rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should now be dead"){
                REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
            }

        }
    }

    GIVEN("A seed where overpopulation (> 3 neighbours) would occur in ALL directions for cell in middle"){
        fileName = "../test/seeds/rules_overpopulation_seed_alldir.txt";
        Population population;
        population.initiatePopulation("von_neumann");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Von Neumann rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should still be alive since the rule only checks cardinal directions"){
                REQUIRE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where underpopulation (< 2 neighbours) would occur in CARDINAL directions for a cell in the middle"){
        fileName = "../test/seeds/rules_underpopulation_seed_cardinaldir.txt";
        Population population;
        population.initiatePopulation("von_neumann");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Von Neumann rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should be dead from underpopulation"){
                REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where underpopulation (< 2 neighbours) would occur in ALL directions for cell in middle"){
        fileName = "../test/seeds/rules_underpopulation_seed_alldir.txt";
        Population population;
        population.initiatePopulation("von_neumann");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Von Neumann rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should still have died from underpopulation"){
                REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where the dead cell in the middle would be resurrected with exactly 3 neighbours in CARDINAL directions"){
        fileName = "../test/seeds/rules_resurrection_seed_cardinaldir.txt";
        Population population;
        population.initiatePopulation("von_neumann");

        THEN("The cell in the center should be dead"){
            REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Von Neumann rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should now be alive from resurrection"){
                REQUIRE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where the dead cell in the middle would be resurrected with exactly 3 neighbours in ALL directions"){
        fileName = "../test/seeds/rules_resurrection_seed_alldir.txt";
        Population population;
        population.initiatePopulation("von_neumann");

        THEN("The cell in the center should be dead"){
            REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Von Neumann rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should stay dead - the 3 neighbours weren't all in cardinal directions"){
                REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where a live cell has enough neighbours to survive (checking CARDINAL directions)"){
        fileName = "../test/seeds/rules_survives_seed_cardinaldir.txt";
        Population population;
        population.initiatePopulation("von_neumann");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Von Neumann rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The cell should stay alive since enough neighbours exist in cardinal directions"){
                REQUIRE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where a live cell has enough neighbours to survive (checking ALL directions)"){
        fileName = "../test/seeds/rules_survives_seed_alldir.txt";
        Population population;
        population.initiatePopulation("von_neumann");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Von Neumann rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The cell should not be alive since only cardinal directions are checked with Von Neumann and they do not contain enough neighbours"){
                REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }
}

// Erik-rules check in all directions, just as Conway
SCENARIO("Testing the Erik rules"){
    GIVEN("A seed where a live cell in the middle has less than two neighbours in all directions"){
        fileName = "../test/seeds/rules_underpopulation_seed_alldir.txt";
        Population population;
        population.initiatePopulation("erik");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Erik rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should now be dead"){
                REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where a live cell in the middle has more than three neighbours in all directions"){
        fileName = "../test/seeds/rules_overpopulation_seed_alldir.txt";
        Population population;
        population.initiatePopulation("erik");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Erik rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The middle cell should now be dead"){
                REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where a dead cell in the middle has exactly three neighbours (checking all directions)"){
        fileName = "../test/seeds/rules_resurrection_seed_alldir.txt";
        Population population;
        population.initiatePopulation("erik");

        THEN("The cell in the center should be dead"){
            REQUIRE_FALSE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Erik rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The cell should now be alive"){
                REQUIRE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed where a live cell has enough neighbours to survive (checking all directions)"){
        fileName = "../test/seeds/rules_survives_seed_alldir.txt";
        Population population;
        population.initiatePopulation("erik");

        THEN("The cell in the center should be alive"){
            REQUIRE(population.getCellAtPosition({2,2}).isAlive());
        }

        WHEN("Advancing two generations with the Erik rule"){
            population.calculateNewGeneration();
            population.calculateNewGeneration();

            THEN("The cell should still be alive thanks to its neighbours"){
                REQUIRE(population.getCellAtPosition({2,2}).isAlive());
            }
        }
    }

    GIVEN("A seed that is infinite (never has the cells die naturally)"){
        fileName = "../test/seeds/rules_infinite_seed.txt";
        Population population;
        population.initiatePopulation("erik");

        THEN("All cells should be alive"){
            bool allAreAlive = true;
            for (int row = 1; row < WORLD_DIMENSIONS.HEIGHT; row++){
                for (int column = 1; column < WORLD_DIMENSIONS.WIDTH; column++){
                    if (population.getCellAtPosition({column, row}).isAlive() != true){
                        allAreAlive = false;
                    }
                }
            }

            REQUIRE(allAreAlive);
        }

        WHEN("Calculating two generations"){
            for (int i = 0; i < 2; i++){
                population.calculateNewGeneration();
            }

            THEN("The cells should still be alive"){
                bool allAreAlive = true;

                for (int row = 1; row < WORLD_DIMENSIONS.HEIGHT; row++){
                    for (int column = 1; column < WORLD_DIMENSIONS.WIDTH; column++){
                        if (population.getCellAtPosition({column, row}).isAlive() != true){
                            allAreAlive = false;
                        }
                    }
                }

                REQUIRE(allAreAlive);
            }

            WHEN("Calculating another four generations"){
                for (int i = 0; i < 4; i++){
                    population.calculateNewGeneration();
                }

                THEN("The cells should still be alive"){
                    bool allAreAlive = true;

                    for (int row = 1; row < WORLD_DIMENSIONS.HEIGHT; row++){
                        for (int column = 1; column < WORLD_DIMENSIONS.WIDTH; column++){
                            if (population.getCellAtPosition({column, row}).isAlive() != true){
                                allAreAlive = false;
                            }
                        }
                    }

                    REQUIRE(allAreAlive);
                }

                THEN("All cells should be OLD (have the color CYAN)"){
                    bool allAreOld = true;

                    for (int row = 1; row < WORLD_DIMENSIONS.HEIGHT; row++){
                        for (int column = 1; column < WORLD_DIMENSIONS.WIDTH; column++){
                            if (population.getCellAtPosition({column, row}).getColor() != STATE_COLORS.OLD){
                                allAreOld = false;
                            }
                        }
                    }

                    REQUIRE(allAreOld);
                }

                THEN("No cell should yet be an ELDER (very old cell)"){
                    bool elderCellExists = false;

                    for (int row = 1; row < WORLD_DIMENSIONS.HEIGHT; row++){
                        for (int column = 1; column < WORLD_DIMENSIONS.WIDTH; column++){
                            if (population.getCellAtPosition({column, row}).getCellValue() == 'E'){
                                elderCellExists = true;
                            }
                        }
                    }

                    REQUIRE_FALSE(elderCellExists);
                }

                WHEN("Calculating another five generations"){
                    for (int i = 0; i < 5; i++){
                        population.calculateNewGeneration();
                    }

                    THEN("All cells should be have the value 'E' (be elder cells)"){
                        bool allAreElder = true;

                        for (int row = 1; row < WORLD_DIMENSIONS.HEIGHT; row++){
                            for (int column = 1; column < WORLD_DIMENSIONS.WIDTH; column++){
                                if (population.getCellAtPosition({column, row}).getCellValue() != 'E'){
                                    allAreElder = false;
                                }
                            }
                        }

                        REQUIRE(allAreElder);
                    }

                    THEN("Only one prime elder should exist (have the colour magenta)"){
                        int amountOfPrimeElders = 0;

                        for (int row = 1; row <= WORLD_DIMENSIONS.HEIGHT; row++){
                            for (int column = 1; column <= WORLD_DIMENSIONS.WIDTH; column++){
                                if (population.getCellAtPosition({column, row}).getColor() == STATE_COLORS.ELDER){
                                    amountOfPrimeElders++;
                                }
                            }
                        }

                        REQUIRE(amountOfPrimeElders == 1);

                        THEN("The prime cell should also be the first cell that was updated with rule in the map (position 1,1)"){
                            REQUIRE(population.getCellAtPosition({1, 1}).getColor() == STATE_COLORS.ELDER);
                        }
                    }
                }
            }
        }
    }
}

//Eva added test for
SCENARIO("Testing the lifespan stages of a cell in Erik"){
    GIVEN("A random population and default world size"){
        fileName="";
        WORLD_DIMENSIONS.HEIGHT=24;
        WORLD_DIMENSIONS.WIDTH=80;
        Population erikPop;
        erikPop.initiatePopulation("erik");

        WHEN("Population ages 100 generations"){
            //save references to watched cells
            map<Point,Cell*> myOldies;
            map<Point,Cell*> myElders;
            Cell* myPrimeElder= nullptr;

            while(erikPop.calculateNewGeneration() < 100) {
                //inside here test are going be be performed as certain events are caught
                //First test cells im following---------------------------------------------------

                //Test oldie cells
                for (auto it = myOldies.begin(); it != myOldies.end(); it++) {
                    if (!it->second->isAlive()) {
                        //cell is now dead
                        THEN("If old cell has died it should have reset it's color") {
                            REQUIRE(it->second->getColor() == STATE_COLORS.DEAD);
                        }
                        THEN("The dead cell should have the age 0"){
                            REQUIRE(it->second->getAge() == 0);
                        }
                    }else
                    {
                        THEN("Any cells older than 4 should have the 'old' color") {
                            REQUIRE(it->second->getColor() == STATE_COLORS.OLD);
                        }
                        THEN("Any cells younger than 10 should have a normal value") {
                            REQUIRE(it->second->getCellValue() == '#');
                        }
                    }
                }

                //test elder cells
                for (auto it = myElders.begin(); it != myElders.end(); it++) {
                    Cell* elderAsses = it->second;

                    if (!elderAsses->isAlive()) {
                        //cell is now dead
                        THEN("If elder cell has died it should have reset it's color") {
                            REQUIRE(it->second->getColor() == STATE_COLORS.DEAD);
                        }
                        THEN("If elder cell has died it should have reset it's value") {
                            REQUIRE(it->second->getCellValue() == '#');
                        }
                        THEN("The dead elder cell should have the age 0"){
                            REQUIRE(it->second->getAge() == 0);
                        }
                    }
                    else{
                        if(elderAsses != myPrimeElder){
                            THEN("Any cells older than 4 should have the 'old' color") {
                                REQUIRE(it->second->getColor() == STATE_COLORS.OLD);
                            }
                        }
                        THEN("Any cells older than 9 should have an E value") {
                            REQUIRE(it->second->getCellValue() == 'E');
                        }
                    }
                }

                //Test the prime elder
                if (myPrimeElder != nullptr) {
                    if (!myPrimeElder->isAlive()) {
                        //The elder died
                        THEN("The dead prime elder cell should have the age 0"){
                            REQUIRE(myPrimeElder->getAge() == 0);
                        }

                        THEN("If prime elder cell has died it should have reset it's value") {
                            REQUIRE(myPrimeElder->getCellValue() == '#');
                        }

                        THEN("If prime elder cell has died it should have reset it's color") {
                            REQUIRE(myPrimeElder->getColor() == STATE_COLORS.DEAD);
                        }
                    } else {
                        //elder is still alive
                        THEN("If prime elder cell it should have 'E' value") {
                            REQUIRE(myPrimeElder->getCellValue() == 'E');

                            // Test fails in rare cases if there is only one elder cell -> it becomes the prime
                            // elder and it dies directly after being appointed.
                            THEN("If prime elder cell it should have 'elder' color") {
                                REQUIRE(myPrimeElder->getColor() == STATE_COLORS.ELDER);
                            }
                        }
                    }
                }

                //Update cell list----------------------------------------------------------------
                myOldies.clear();
                myElders.clear();
                //Look for new 5-yo's (Are now considered oldies), And new 10-yo's (Now considered elders)
                for (int row = 1; row < WORLD_DIMENSIONS.HEIGHT; row++) {
                    for (int column = 1; column < WORLD_DIMENSIONS.WIDTH; column++) {
                        Cell *itCell = &erikPop.getCellAtPosition({column, row});

                        int cellAge = itCell->getAge();

                        if (cellAge > 4 && cellAge < 10) { //all these should have different color
                            myOldies[Point{column, row}] = itCell;
                        }

                        if (cellAge > 9) { //all these should have value 'E'
                            myElders[Point{column, row}] = itCell;
                        }
                    }
                }

                //which one is elder?
                for (auto it = myElders.begin(); it != myElders.end(); it++) {
                    if(it->second->getNextGenerationAction() != KILL_CELL){ //cell wouldn't have been picked if it was going to die
                        if(myPrimeElder== nullptr || it->second->getAge() > myPrimeElder->getAge()){
                            myPrimeElder = it->second;
                        }
                    }
                }
            }
        }
    }
}

